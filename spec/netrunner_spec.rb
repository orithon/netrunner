# frozen_string_literal: true

require "spec_helper"

RSpec.describe Netrunner do
  it "has a version number" do
    expect(Netrunner::VERSION).not_to be nil
  end
end
