# frozen_string_literal: true

require 'spec_helper'
require 'netrunner/piece'

RSpec.describe Piece do
  let(:piece) { Piece.new }

  describe 'a standalone piece' do
    it 'has no parent piece'

    context 'when no zones have been set up' do
      it 'has no zones'
    end

    context 'when zones have been set up' do
      it 'has a set of zones'

      context 'when other pieces have been added to its zones' do
        # Useful for cards like parasite, which needs to check how much strength
        # its parent has.
        it 'the other pieces have the main piece as a parent'
        it 'the main piece knows if it has a certain piece when queried'
      end
    end
  end
end
