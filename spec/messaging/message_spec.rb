# frozen_string_literal: true

require 'spec_helper'
require 'netrunner/messaging/message'

RSpec.describe Message do
  let(:message) { Message.new(type: type, payload: payload) }
  let(:type) { :note }
  let(:payload) { nil }

  it 'has a type and a payload' do
    expect(message).to have_attributes(type: type, payload: payload)
  end
end
