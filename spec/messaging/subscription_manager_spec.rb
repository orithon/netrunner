require 'spec_helper'
require 'netrunner/messaging/subscription_manager'
require 'netrunner/messaging/subscription'
require 'netrunner/messaging/message'

RSpec.describe SubscriptionManager do
  let(:subscription_manager) { SubscriptionManager.instance }
  let(:message) { instance_double(Message, type: :message) }

  let(:subscription) do
    s = instance_double(Subscription, message: :message, hash: 1, receive: nil)
    allow(s).to receive(:matches?) { |msg| s.message == msg.type }
    allow(s).to receive(:eql?) { |sub| s.hash == sub.hash }
    s
  end

  let(:equivalent_subscription) do
    s = instance_double(Subscription, message: :message, hash: 1, receive: nil)
    allow(s).to receive(:eql?) { |sub| s.hash == sub.hash }
    s
  end

  let(:different_subscription) do
    s = instance_double(Subscription, message: :note, hash: 2, receive: nil)
    allow(s).to receive(:matches?) { |msg| s.message == msg.type }
    s
  end

  after(:each) { subscription_manager.clear }

  it 'is initialized with no subscriptions' do
    expect(subscription_manager.subscriptions).to be_empty
  end

  it 'can have subscriptions added to it' do
    subscription_manager.add(subscription)
    expect(subscription_manager.subscriptions).to contain_exactly(subscription)
  end

  it 'will ignore added subscriptions that it already has' do
    subscription_manager.add(subscription)
    subscription_manager.add(equivalent_subscription)
    expect(subscription_manager.subscriptions).to contain_exactly(subscription)
  end

  it 'will allow additional subscriptions that it does not have' do
    subscription_manager.add(subscription)
    subscription_manager.add(different_subscription)
    expect(subscription_manager.subscriptions).to contain_exactly(subscription, different_subscription)
  end

  it 'can have subscriptions removed from it' do
    subscription_manager.add(subscription)
    subscription_manager.remove(subscription)
    expect(subscription_manager.subscriptions).to be_empty
  end

  it 'can send a message to subscribers that want the message' do
    expect(subscription).to receive(:receive).with(message)
    subscription_manager.add(subscription)
    subscription_manager.deliver(message)
  end
end
