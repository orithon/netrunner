require 'spec_helper'
require 'netrunner/messaging/subscription'
require 'netrunner/messaging/message'

RSpec.describe Subscription do
  let(:subscription) do
    Subscription.new(message: :message, receiver: receiver, action: :handler)
  end

  let(:equivalent_subscription) do
    Subscription.new(message: :message, receiver: receiver, action: :handler)
  end

  let(:different_subscription_by_message) do
    Subscription.new(message: :note, receiver: receiver, action: :handler)
  end

  let(:different_subscription_by_receiver) do
    Subscription.new(message: message, receiver: different_receiver,
                     action: :handler)
  end

  let(:different_subscription_by_action) do
    Subscription.new(message: message, receiver: receiver, action: :class)
  end

  let(:receiver) { spy('Receiver') }

  let(:different_receiver) { spy('OtherReceiver') }

  let(:message) { instance_double(Message, type: :message) }
  let(:note) { instance_double(Message, type: :note) }

  it 'will match messages that it wants' do
    expect(subscription.matches?(message)).to be true
  end

  it 'will not match messages that it does not want' do
    expect(subscription.matches?(note)).to be false
  end

  it 'will be equal to another subscription with the same parameters' do
    expect(subscription).to eq equivalent_subscription
  end

  it 'will not be equal to a subscription that wants a different message' do
    expect(subscription).to_not eq different_subscription_by_message
  end

  it 'will not be equal to a subscription that has a different receiver' do
    expect(subscription).to_not eq different_subscription_by_receiver
  end

  it 'will not be equal to a subscription that has a different action' do
    expect(subscription).to_not eq different_subscription_by_action
  end

  it 'will have the hash of a subscription with the same parameters' do
    expect(subscription.hash).to eq equivalent_subscription.hash
  end

  it 'will not have the hash of a subscription with a different message' do
    expect(subscription.hash).to_not eq different_subscription_by_message.hash
  end

  it 'will not have the hash of a subscription with a different receiver' do
    expect(subscription.hash).to_not eq different_subscription_by_receiver.hash
  end

  it 'will not have the hash of a subscription with a different action' do
    expect(subscription.hash).to_not eq different_subscription_by_action.hash
  end

  it 'will act upon received messages that it wants' do
    subscription.receive(message)
    expect(subscription.receiver).to have_received(:handler).with(message)
  end
end
