# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'netrunner/version'

Gem::Specification.new do |spec|
  spec.name          = 'netrunner'
  spec.version       = Netrunner::VERSION
  spec.authors       = ['Seth Armstrong']
  spec.email         = ['sethjarmstrong@gmail.com']

  spec.summary       = %q{A ruby implementation of an Android: Netrunner rules engine.}
  spec.homepage      = 'https://bitbucket.org/orithon/netrunner/'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the
  # 'allowed_push_host' to allow pushing to a single host or delete this
  # section to allow pushing to any host.
  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)

  spec.metadata['allowed_push_host'] = 'TODO: Set to "http://mygemserver.com"'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'aruba'
  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'cucumber', '~> 2'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3'
end
