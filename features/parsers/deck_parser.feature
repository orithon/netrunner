Feature: DeckParser
  The DeckParser needs to take a string representing a Netrunner deck and
  successfully create a "deck" of cards: that is, some data structure that
  contains all of the cards that are in the deck listing, with the proper
  quantity of each card.

  The "deck" is going to be part of a "player", which consists of, on
  initialization:
  1. An identity card.
  2. A basic action card.
  3. A collection of clicks.
  4. A collection of credits.
  5. A deck.

  At it's most basic, a "deck" may simply be an array that can be "shuffled"
  (ranedomized). Drawing a card can simply be popping the first card off the
  top.

  There are some rules with regards to deck construction:
  1. A deck with a Runner identity may only contain Runner cards.
  2. A deck with a Corporation identity may only contain Corporation cards.
  3. A deck must be associated with a single identity card.
  4. A deck cannot contain fewer cards than the minimum deck size value listed
     on the chosen identity card.
  5. A deck cannot have more than three copies of a single card (by title).
  6. A deck cannot contain out-of-faction cards with a total influence value
     that exceeds the influence limit listed on the chosen identity card.
  7. A Corporation deck must have a specific number of agenda points in it based
     on the size of the deck, as follows:
     - 40 to 44 cards requires 18 or 19 agenda points.
     - 45 to 49 cards requires 20 or 21 agenda points.
     - 50 to 54 cards requires 22 or 23 agenda points.
     For decks larger than this, add 2 additional agenda points to the 54 card
     deck requirements each time the number of cards in the deck reaches a
     multiple of 5 (55, 60, 65, etc.).
     FORMULA: (floor(Cards / 5) + 1) * 2, (floor(Cards / 5) + 1) * 2 + 1

  Scenario: A perfectly normal Runner deck
    Given there is a deck file for "kate mac mccaffrey"
    When the deck file is parsed
    Then the deck is generated successfully
    And the deck's identity card is "Kate "Mac" McCaffrey: Digital Tinker"
    And the deck contains 3 Diesel cards
    And the deck contains 3 Infiltration cards
    And the deck contains 2 Modded cards
    And the deck contains 3 Sure Gamble cards
    And the deck contains 3 The Maker's Eye cards
    And the deck contains 3 Tinkering cards
    And the deck contains 2 Akamatsu Mem Chip cards
    And the deck contains 2 Rabbit Hole cards
    And the deck contains 2 The Personal Touch cards
    And the deck contains 1 The Toolbox card
    And the deck contains 3 Access to Globalsec cards
    And the deck contains 1 Aesop's Pawnshop card
    And the deck contains 3 Armitage Codebusting cards
    And the deck contains 2 Sacrificial Construct cards
    And the deck contains 2 Battering Ram cards
    And the deck contains 3 Crypsis cards
    And the deck contains 3 Gordian Blade cards
    And the deck contains 2 Pipeline cards
    And the deck contains 2 Magnum Opus cards
    And the deck contains 2 Net Shield cards

  Scenario: A perfectly normal Corporation deck
    Given there is a deck file for "jinteki personal evolution"
    When the deck file is parsed
    Then the deck is generated successfully
    And the deck's identity card is "Jinteki: Personal Evolution"
    And the deck contains 3 Nisei MK II cards
    And the deck contains 3 Priority Requisition cards
    And the deck contains 3 Private Security Force cards
    And the deck contains 2 Melange Mining Corp. cards
    And the deck contains 3 PAD Campaign cards
    And the deck contains 3 Project Junebug cards
    And the deck contains 3 Snare! cards
    And the deck contains 1 Zaibatsu Loyalty card
    And the deck contains 1 Akitaro Watanabe card
    And the deck contains 3 Hedge Fund cards
    And the deck contains 2 Neural EMP cards
    And the deck contains 2 Precognition cards
    And the deck contains 3 Wall of Static cards
    And the deck contains 3 Wall of Thorns cards
    And the deck contains 2 Cell Portal cards
    And the deck contains 2 Chum cards
    And the deck contains 3 Enigma cards
    And the deck contains 2 Hunter cards
    And the deck contains 3 Neural Katana cards
    And the deck contains 2 Data Mine cards

  Scenario: A deck without an identity card
    Given there is a deck file for "no one"
    When the deck file is parsed
    Then the deck parser complains that no identity was given

  Scenario: A deck with more than one identity card
    Given there is a deck file for "multiple personalities"
    When the deck file is parsed
    Then the deck parser complains that more than one identity was given

  Scenario: A Runner deck with Corporation cards
    Given there is a deck file for "corporate sell out"
    When the deck file is parsed
    Then the deck parser complains that corporation cards have been included in a runner deck

  Scenario: A Corporation deck with Runner cards
    Given there is a deck file for "cool kid wannabe"
    When the deck file is parsed
    Then the deck parser complains that runner cards have been included in a corporation deck

  Scenario: A deck with too few cards
    Given there is a deck file for "running without a full deck"
    When the deck file is parsed
    Then the deck parser complains that there are not enough cards in the deck

  Scenario: A deck with too many copies of a card
    Given there is a deck file for "but i love that card"
    When the deck file is parsed
    Then the deck parser complains that there are too many copies of a card

  Scenario: A deck with too many out-of-faction cards
    Given there is a deck file for "unbelievable alliance"
    When the deck file is parsed
    Then the deck parser complains that there are too many out of faction cards

  Scenario: A deck with out-of-faction cards with no influence value
    Given there is a deck file for "impossible alliance"
    When the deck file is parsed
    Then the deck parser complains that there are faction specific cards the identity cannot use

  Scenario: A Corporation deck with too few agenda points
    Given there is a deck file for "aimless corporation"
    When the deck file is parsed
    Then the deck parser complains that there are too few agenda points

  Scenario: A Corporation deck with too many agenda points
    Given there is a deck file for "overachiever"
    When the deck file is parsed
    Then the deck parser complains that there are too many agenda points

  Scenario: An empty deck
    Given there is a deck file for "emptiness"
    When the deck file is parsed
    Then the deck parser complains that the deck file was empty

  Scenario: A deck with cards that don't exist
    Given there is a deck file for "imaginary cards"
    When the deck file is parsed
    Then the deck parser complains that the cards do not exist
