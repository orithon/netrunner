Feature: Messaging
  In order to communicate and coordinate with one another, objects should be
  able to send messages to each other, and to act upon those messages

  Background:
    Given the subscriptions list is empty

  Scenario: Collecting message
    Given there are 2 collectable objects
    When a collection message for the objects is sent
    Then the message payload contains the collectable objects
