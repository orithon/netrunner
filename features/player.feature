Feature: Player
  Background:
    # Assume we have a runner using "kate mac mccaffrey"
    # and a corporation using "jinteki personal evolution"
    Given the game has been initialized

  Scenario: The Runner has basic credits
    Given the runner has been given 5 credits
    Then the runner has 5 credits

  Scenario: The Runner has icebreaker credits
    # TODO: Look at cards in the core set that have limited credits, and write
    # specific tests for them.  It may be the job of the player to know how many
    # credits they have, but limited credits should be counted outside of that
    # pool.
    Given the runner has been given 5 credits
    And the runner has been given 2 icebreaker credits
    Then the runner has 5 credits

  Scenario: The Runner's clicks
    Given the runner has been given 4 clicks
    Then the runner has 4 clicks

  Scenario: The Runner's deck has been shuffled
    Then the runner deck has been shuffled

  Scenario: The Runner's link
    # TODO: Write specific tests for cards that increase the runner's link
    Then the runner has 1 link
