Feature: Game Play
  When a game of Netrunner is initialized it should be passed the deck lists of
  both players, perhaps along with other useful meta-data (usernames, user IDs,
  whether one or both players are AIs?).

  Decklistings should be in a simple format, which the code can read, convert
  into a list of card objects, and then validate based on the meta-data the app
  has on the cards.

  Jinteki.net's format is simple enough to use within the app, but it lacks the
  identity card... Perhaps the identity should be a separate argument?  Nah,
  let's just include it at the top of the deck list, and this app will
  effectively use a modified Jinteki.net format.

  The web interface part of the app (future work) should handle format
  conversion to convert other deck list formats into one that the back end can
  handle.

  Once the game is initialized then the game should have two players, each with
  their identity, deck, and basic action card in play, as well as 5 credits
  each. The runner should have 4 actions, and the corporation should have 3.

  The first event on the event stack should be GAME_START, and the game phase
  should be GAME_START (human-readable as "Game Start").

  The phases should all be in the phase manager, in this order, with these
  events:
    1. Game Start
      1. GAME_START
    2. Corporation's Draw Phase
      1. BEGIN_CORP_DRAW          - Corp's draw phase begins
      2. CORP_ABILITY_REZ_SCORE   - Paid abilities can be triggered, Cards can be rezzed, Agendas can be scored
      3. BEGIN_CORP_TURN          - Turn begins ("When your turn begins" conditionals meet their trigger conditions)
      4. CORP_DRAW                - Draw one card
      5. CORP_ABILITY_REZ_SCORE   - Paid abilities can be triggered, Cards can be rezzed, Agendas can be scored
      6. END_CORP_DRAW            - Corp's draw phase ends
    3. Corporation's Action Phase
      1. BEGIN_CORP_ACTION        - Corp's action phase begins
      2. CORP_ACTION              - Take an action
      3. CORP_ABILITY_REZ_SCORE   - Paid abilities can be triggered, Cards can be rezzed, Agendas can be scored
      4. END_CORP_ACTION          - Corp's action phase ends (return to step 1 if the corp has more actions)
    4. Corporation's Discard Phase
      1. BEGIN_CORP_DISCARD       - Corp's discard phase begins
      2. CORP_DISCARD             - Discard down to maximum hand size
      3. CORP_ABILITY_REZ         - Paid abilities can be triggered, Cards can be rezzed
      4. END_CORP_TURN            - End of turn
      5. END_CORP_DISCARD         - Corp's discard phase ends
    5. Runner's Start Phase
      1. BEGIN_RUNNER_START       - Runner's start phase begins
      2. RUNNER_ABILITY_REZ       - Paid abilities can be triggered, Cards can be rezzed
      3. BEGIN_RUNNER_TURN        - Turn begins ("When your turn begins" conditionals meet their trigger conditions)
      4. RUNNER_ABILITY_REZ       - Paid abilities can be triggered, Cards can be rezzed
      5. END_RUNNER_START         - Runner's start phase ends
    6. Runner's Action Phase
      1. BEGIN_RUNNER_ACTION      - Runner's action phase begins
      2. RUNNER_ACTION            - Take an action
      3. RUNNER_ABILITY_REZ       - Paid abilities can be triggered, Cards can be rezzed
      4. END_RUNNER_ACTION        - Runner's action phase ends (return to step 1 if the runner has more actions)
    7. Runner's Discard Phase
      1. BEGIN_RUNNER_DISCARD     - Runner's discard phase begins
      2. RUNNER_DISCARD           - Discard down to maximum hand size
      3. RUNNER_ABILITY_REZ       - Paid abilities can be triggered, Cards can be rezzed
      4. END_RUNNER_TURN          - End of turn
      5. END_RUNNER_DISCARD       - Runner's discard phase ends

  The "Game Start" phase should trigger, and each player should draw 5 cards, and be offered a mulligan.

  Background:
    # Assume the decks "kate mac mccaffrey" and "jinteki personal evolution"
    Given the game has been initialized

  Scenario: Initialization
    Then the corporation has 5 credits
     And the corporation has 3 clicks
     And the corporation deck has been shuffled
     And the corporation has 5 cards in their hand
     And the corporation identity is in play
     And the runner has 5 credits
     And the runner has 4 clicks
     And the runner deck has been shuffled
     And the runner has 5 cards in their hand
     And the runner identity is in play

  Scenario: Playing a Hedge Fund
    Given the corporation has a Hedge Fund in their hand
      And the phase is Corporation's Action Phase
     Then the corporation has 5 credits
      And the corporation has 3 clicks
    Given the game is processed
     When the corporation chooses to play an operation
      And the corporation plays the card in their hand
     Then the corporation has 9 credits
      And the corporation has 2 clicks
      And the card is now in archives
      And the current phase is Corporation's Action Phase

  Scenario: Playing a Medium using a credit from Cyberfeeder
    Given the runner has a Cyberfeeder in play
      And the runner has a Medium in their hand
      And the phase is Runner's Action Phase
     Then the runner has 5 credits
      And the runner has 1 recurring credit that has not been spent
      # TODO: And the runner has 1 credit for using icebreakers
      And the runner has 6 credits for installing virus programs
      And the runner has 4 clicks
    Given the game is processed
     When the runner chooses to install a card
      And the runner installs the card in their hand
      And the runner is presented with the credits they could pay with
      And the credits the runner is presented with includes 1 recurring credit
      And the runner spends the credit on Cyberfeeder
     Then the runner has 3 credits
      And the runner has 1 recurring credit that has been spent
      # TODO: And the runner has 0 credits for using icebreakers
      And the runner has 3 credits for installing virus programs
      And the runner has 3 clicks
      And the card is now in the program rig
