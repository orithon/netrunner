Feature: Aesop's Pawnshop
  Background:
    Given the card is "Aesop's Pawnshop"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card is unique
    And the card has the name "Aesop's Pawnshop"
    And the card has the subtypes "Location, Connection"
    And the card has the faction "Shaper"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has the text
      """
      When your turn begins, you may trash another of your installed cards to gain 3{credit}.
      """
