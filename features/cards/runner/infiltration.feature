Feature: Infiltration
  Background:
    Given the card is "Infiltration"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Infiltration"
    And the card has the faction "Neutral"
    And the card has a cost of 0
    And the card has an influence cost of 0
    And the card has the text
      """
      Gain 2{credit} or expose 1 card.
      """
