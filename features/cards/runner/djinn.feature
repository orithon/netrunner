Feature: Djinn
  Background:
    Given the card is "Djinn"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Djinn"
    And the card has the subtypes "Daemon"
    And the card has the faction "Anarch"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has a ram cost of 1
    And the card has the text
      """
      Djinn can host up to 3{ram} of non-<b>icebreaker</b> programs.
      The memory costs of hosted programs do not count against your memory limit.
      {click}, 1{credit}: Search your stack for a <b>virus</b> program, reveal it, and add it to your grip. Shuffle your stack.
      """
