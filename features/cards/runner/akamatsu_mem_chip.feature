Feature: Akamatsu Mem Chip
  Background:
    Given the card is "Akamatsu Mem Chip"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card has the name "Akamatsu Mem Chip"
    And the card has the subtypes "Chip"
    And the card has the faction "Shaper"
    And the card has a cost of 1
    And the card has an influence cost of 1
    And the card has the text
      """
      +1{ram}
      """
