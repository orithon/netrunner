Feature: Magnum Opus
  Background:
    Given the card is "Magnum Opus"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Magnum Opus"
    And the card has the faction "Shaper"
    And the card has a cost of 5
    And the card has an influence cost of 2
    And the card has a ram cost of 2
    And the card has the text
      """
      {click}: Gain 2{credit}.
      """
