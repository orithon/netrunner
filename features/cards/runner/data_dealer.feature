Feature: Data Dealer
  Background:
    Given the card is "Data Dealer"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Data Dealer"
    And the card has the subtypes "Connection, Seedy"
    And the card has the faction "Criminal"
    And the card has a cost of 0
    And the card has an influence cost of 2
    And the card has the text
      """
      <b>Forfeit an agenda</b>, {click}: Gain 9{credit}.
      """
