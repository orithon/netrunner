Feature: Modded
  Background:
    Given the card is "Modded"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Modded"
    And the card has the subtypes "Mod"
    And the card has the faction "Shaper"
    And the card has a cost of 0
    And the card has an influence cost of 2
    And the card has the text
      """
      Install a program or a piece of hardware, lowering the install cost by 3.
      """
