Feature: Net Shield
  Background:
    Given the card is "Net Shield"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Net Shield"
    And the card has the faction "Shaper"
    And the card has a cost of 2
    And the card has an influence cost of 1
    And the card has a ram cost of 1
    And the card has the text
      """
      1{credit}: Prevent the first net damage this turn.
      """
