Feature: Sneakdoor Beta
  Background:
    Given the card is "Sneakdoor Beta"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Sneakdoor Beta"
    And the card has the faction "Criminal"
    And the card has a cost of 4
    And the card has an influence cost of 3
    And the card has a ram cost of 2
    And the card has the text
      """
      {click}: Make a run on Archives. If successful, instead treat it as a successful run on HQ.
      """
