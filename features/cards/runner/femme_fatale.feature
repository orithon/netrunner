Feature: Femme Fatale
  Background:
    Given the card is "Femme Fatale"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Femme Fatale"
    And the card has the subtypes "Icebreaker, Killer"
    And the card has the faction "Criminal"
    And the card has a cost of 9
    And the card has an influence cost of 1
    And the card has a ram cost of 1
    And the card has a strength of 2
    And the card has the text
      """
      1{credit}: Break <b>sentry</b> subroutine.
      2{credit}: +1 strength.
      When you install Femme Fatale, choose an installed piece of ice. When you encounter that ice, you may pay 1{credit} per subroutine on that ice to bypass it.
      """
