Feature: Crash Space
  Background:
    Given the card is "Crash Space"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Crash Space"
    And the card has the subtypes "Location"
    And the card has the faction "Criminal"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has the text
      """
      2{recurring_credit}
      Use these credits to pay for removing tags.
      {trash}: Prevent up to 3 meat damage.
      """
