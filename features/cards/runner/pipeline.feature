Feature: Pipeline
  Background:
    Given the card is "Pipeline"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Pipeline"
    And the card has the subtypes "Icebreaker, Killer"
    And the card has the faction "Shaper"
    And the card has a cost of 3
    And the card has an influence cost of 1
    And the card has a ram cost of 1
    And the card has a strength of 1
    And the card has the text
      """
      1{credit}: Break <b>sentry</b> subroutine.
      2{credit}: +1 strength for the remainder of this run.
      """
