Feature: Battering Ram
  Background:
    Given the card is "Battering Ram"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Battering Ram"
    And the card has the subtypes "Icebreaker, Fracter"
    And the card has the faction "Shaper"
    And the card has a cost of 5
    And the card has an influence cost of 2
    And the card has a ram cost of 2
    And the card has a strength of 3
    And the card has the text
      """
      2{credit}: Break up to 2 <b>barrier</b> subroutines.
      1{credit}: +1 strength for the remainder of this run.
      """
