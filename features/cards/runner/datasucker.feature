Feature: Datasucker
  Background:
    Given the card is "Datasucker"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Datasucker"
    And the card has the subtypes "Virus"
    And the card has the faction "Anarch"
    And the card has a cost of 1
    And the card has an influence cost of 1
    And the card has a ram cost of 1
    And the card has the text
      """
      Whenever you make a successful run on a central server, place 1 virus counter on Datasucker.
      <b>Hosted virus counter</b>: Rezzed piece of ice currently being encountered has -1 strength until the end of the encounter.
      """
