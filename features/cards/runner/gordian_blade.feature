Feature: Gordian Blade
  Background:
    Given the card is "Gordian Blade"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Gordian Blade"
    And the card has the subtypes "Icebreaker, Decoder"
    And the card has the faction "Shaper"
    And the card has a cost of 4
    And the card has an influence cost of 3
    And the card has a ram cost of 1
    And the card has a strength of 2
    And the card has the text
      """
      1{credit}: Break <b>code gate</b> subroutine.
      1{credit}: +1 strength for the remainder of this run.
      """
