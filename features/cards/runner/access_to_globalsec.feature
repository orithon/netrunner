Feature: Access to Globalsec
  Background:
    Given the card is "Access to Globalsec"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Access to Globalsec"
    And the card has the subtypes "Link"
    And the card has the faction "Neutral"
    And the card has a cost of 1
    And the card has an influence cost of 0
    And the card has the text
      """
      +1{link}
      """
