Feature: Ninja
  Background:
    Given the card is "Ninja"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Ninja"
    And the card has the subtypes "Icebreaker, Killer"
    And the card has the faction "Criminal"
    And the card has a cost of 4
    And the card has an influence cost of 2
    And the card has a ram cost of 1
    And the card has a strength of 0
    And the card has the text
      """
      1{credit}: Break <b>sentry</b> subroutine.
      3{credit}: +5 strength.
      """
