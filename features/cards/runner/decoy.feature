Feature: Decoy
  Background:
    Given the card is "Decoy"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Decoy"
    And the card has the subtypes "Connection"
    And the card has the faction "Criminal"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has the text
      """
      {trash}: Avoid receiving 1 tag.
      """
