Feature: The Toolbox
  Background:
    Given the card is "The Toolbox"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card is unique
    And the card has the name "The Toolbox"
    And the card has the subtypes "Console"
    And the card has the faction "Shaper"
    And the card has a cost of 9
    And the card has an influence cost of 2
    And the card has the text
      """
      +2{ram} +2{link}
      2{recurring_credit}
      Use these credits to pay for using <b>icebreakers</b>.
      Limit 1 <b>console</b> per player.
      """
