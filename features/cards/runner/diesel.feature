Feature: Diesel
  Background:
    Given the card is "Diesel"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Diesel"
    And the card has the faction "Shaper"
    And the card has a cost of 0
    And the card has an influence cost of 2
    And the card has the text
      """
      Draw 3 cards.
      """
