Feature: Easy Mark
  Background:
    Given the card is "Easy Mark"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Easy Mark"
    And the card has the subtypes "Job"
    And the card has the faction "Criminal"
    And the card has a cost of 0
    And the card has an influence cost of 1
    And the card has the text
      """
      Gain 3{credit}.
      """
