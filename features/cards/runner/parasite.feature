Feature: Parasite
  Background:
    Given the card is "Parasite"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Parasite"
    And the card has the subtypes "Virus"
    And the card has the faction "Anarch"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has a ram cost of 1
    And the card has the text
      """
      Install Parasite only on a rezzed piece of ice.
      Host ice has -1 strength for each virus counter on Parasite and is trashed if its strength is 0 or less.
      When your turn begins, place 1 virus counter on Parasite.
      """
