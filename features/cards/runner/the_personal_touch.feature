Feature: The Personal Touch
  Background:
    Given the card is "The Personal Touch"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card has the name "The Personal Touch"
    And the card has the subtypes "Mod"
    And the card has the faction "Shaper"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has the text
      """
      Install The Personal Touch only on an <b>icebreaker</b>.
      Host <b>icebreaker</b> has +1 strength.
      """
