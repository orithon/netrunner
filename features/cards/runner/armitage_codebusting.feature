Feature: Armitage Codebusting
  Background:
    Given the card is "Armitage Codebusting"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Armitage Codebusting"
    And the card has the subtypes "Job"
    And the card has the faction "Neutral"
    And the card has a cost of 1
    And the card has an influence cost of 0
    And the card has the text
      """
      Place 12{credit} from the bank on Armitage Codebusting when it is installed. When there are no credits left on Armitage Codebusting, trash it.
      {click}: Take 2{credit} from Armitage Codebusting.
      """
