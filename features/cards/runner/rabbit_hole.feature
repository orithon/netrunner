Feature: Rabbit Hole
  Background:
    Given the card is "Rabbit Hole"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card has the name "Rabbit Hole"
    And the card has the subtypes "Link"
    And the card has the faction "Shaper"
    And the card has a cost of 2
    And the card has an influence cost of 1
    And the card has the text
      """
      +1{link}
      When Rabbit Hole is installed, you may search your stack for another copy of Rabbit Hole and install it by paying its install cost. Shuffle your stack.
      """
