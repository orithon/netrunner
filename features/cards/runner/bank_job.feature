Feature: Bank Job
  Background:
    Given the card is "Bank Job"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Bank Job"
    And the card has the subtypes "Job"
    And the card has the faction "Criminal"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has the text
      """
      Place 8{credit} from the bank on Bank Job when it is installed. When there are no credits left on Bank Job, trash it.
      Whenever you make a successful run on a remote server, instead of accessing cards you may take any number of credits from Bank Job.
      """
