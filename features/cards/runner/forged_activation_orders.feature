Feature: Forged Activation Orders
  Background:
    Given the card is "Forged Activation Orders"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Forged Activation Orders"
    And the card has the subtypes "Sabotage"
    And the card has the faction "Criminal"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has the text
      """
      Choose an unrezzed piece of ice. The Corp must either rez that ice or trash it.
      """
