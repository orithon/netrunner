Feature: Stimhack
  Background:
    Given the card is "Stimhack"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Stimhack"
    And the card has the subtypes "Run"
    And the card has the faction "Anarch"
    And the card has a cost of 0
    And the card has an influence cost of 1
    And the card has the text
      """
      Make a run, and gain 9{credit}, which you may use only during this run. After the run is completed, suffer 1 brain damage (cannot be prevented) and return to the bank any of the 9{credit} not spent.
      """
