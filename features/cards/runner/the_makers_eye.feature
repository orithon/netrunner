Feature: The Maker's Eye
  Background:
    Given the card is "The Maker's Eye"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "The Maker's Eye"
    And the card has the subtypes "Run"
    And the card has the faction "Shaper"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has the text
      """
      Make a run on R&D. If successful, access 2 additional cards from R&D.
      """
