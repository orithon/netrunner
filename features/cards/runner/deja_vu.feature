Feature: Déjà Vu
  Background:
    Given the card is "Déjà Vu"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Déjà Vu"
    And the card has the faction "Anarch"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has the text
      """
      Add 1 card (or up to 2 <b>virus</b> cards) from your heap to your grip.
      """
