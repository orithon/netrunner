Feature: Demolition Run
  Background:
    Given the card is "Demolition Run"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Demolition Run"
    And the card has the subtypes "Run, Sabotage"
    And the card has the faction "Anarch"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has the text
      """
      Make a run on HQ or R&D. You may trash, at no cost, any cards you access (even if the cards cannot normally be trashed).
      """
