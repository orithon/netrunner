Feature: Ice Carver
  Background:
    Given the card is "Ice Carver"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Ice Carver"
    And the card has the subtypes "Virtual"
    And the card has the faction "Anarch"
    And the card has a cost of 3
    And the card has an influence cost of 3
    And the card has the text
      """
      All ice is encountered with its strength lowered by 1.
      """
