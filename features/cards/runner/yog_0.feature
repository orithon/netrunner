Feature: Yog.0
  Background:
    Given the card is "Yog.0"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Yog.0"
    And the card has the subtypes "Icebreaker, Decoder"
    And the card has the faction "Anarch"
    And the card has a cost of 5
    And the card has an influence cost of 1
    And the card has a ram cost of 1
    And the card has a strength of 3
    And the card has the text
      """
      0{credit}: Break <b>code gate</b> subroutine.
      """
