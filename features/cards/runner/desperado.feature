Feature: Desperado
  Background:
    Given the card is "Desperado"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card is unique
    And the card has the name "Desperado"
    And the card has the subtypes "Console"
    And the card has the faction "Criminal"
    And the card has a cost of 3
    And the card has an influence cost of 3
    And the card has the text
      """
      +1{ram}
      Gain 1{credit} whenever you make a successful run.
      Limit 1 <b>console</b> per player.
      """
