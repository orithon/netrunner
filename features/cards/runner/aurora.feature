Feature: Aurora
  Background:
    Given the card is "Aurora"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Aurora"
    And the card has the subtypes "Icebreaker, Fracter"
    And the card has the faction "Criminal"
    And the card has a cost of 3
    And the card has an influence cost of 1
    And the card has a ram cost of 1
    And the card has a strength of 1
    And the card has the text
      """
      2{credit}: Break <b>barrier</b> subroutine.
      2{credit}: +3 strength.
      """
