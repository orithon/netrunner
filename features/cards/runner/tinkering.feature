Feature: Tinkering
  Background:
    Given the card is "Tinkering"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Tinkering"
    And the card has the subtypes "Mod"
    And the card has the faction "Shaper"
    And the card has a cost of 0
    And the card has an influence cost of 4
    And the card has the text
      """
      Choose a piece of ice. That ice gains <b>sentry</b>, <b>code gate</b>, and <b>barrier</b> until the end of the turn.
      """
