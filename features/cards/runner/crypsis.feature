Feature: Crypsis
  Background:
    Given the card is "Crypsis"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Crypsis"
    And the card has the subtypes "Icebreaker, AI, Virus"
    And the card has the faction "Neutral"
    And the card has a cost of 5
    And the card has an influence cost of 0
    And the card has a ram cost of 1
    And the card has a strength of 0
    And the card has the text
      """
      1{credit}: Break ice subroutine.
      1{credit}: +1 strength.
      {click}: Place 1 virus counter on Crypsis.
      When an encounter with a piece of ice in which you used Crypsis to break a subroutine ends, remove 1 hosted virus counter or trash Crypsis.
      """
