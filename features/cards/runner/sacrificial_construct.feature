Feature: Sacrificial Construct
  Background:
    Given the card is "Sacrificial Construct"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Sacrificial Construct"
    And the card has the subtypes "Remote"
    And the card has the faction "Shaper"
    And the card has a cost of 0
    And the card has an influence cost of 1
    And the card has the text
      """
      {trash}: Prevent an installed program or an installed piece of hardware from being trashed.
      """
