Feature: Corroder
  Background:
    Given the card is "Corroder"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Corroder"
    And the card has the subtypes "Icebreaker, Fracter"
    And the card has the faction "Anarch"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has a ram cost of 1
    And the card has a strength of 2
    And the card has the text
      """
      1{credit}: Break <b>barrier</b> subroutine.
      1{credit}: +1 strength.
      """
