Feature: Inside Job
  Background:
    Given the card is "Inside Job"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Inside Job"
    And the card has the subtypes "Run"
    And the card has the faction "Criminal"
    And the card has a cost of 2
    And the card has an influence cost of 3
    And the card has the text
      """
      Make a run. Bypass the first piece of ice encountered during this run.
      """
