Feature: Account Siphon
  Background:
    Given the card is "Account Siphon"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Account Siphon"
    And the card has the subtypes "Run, Sabotage"
    And the card has the faction "Criminal"
    And the card has a cost of 0
    And the card has an influence cost of 4
    And the card has the text
      """
      Make a run on HQ. If successful, instead of accessing cards you may force the Corp to lose up to 5{credit}, then you gain 2{credit} for each credit lost and take 2 tags.
      """
