Feature: Cyberfeeder
  Background:
    Given the card is "Cyberfeeder"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card has the name "Cyberfeeder"
    And the card has the subtypes "Chip"
    And the card has the faction "Anarch"
    And the card has a cost of 2
    And the card has an influence cost of 1
    And the card has the text
      """
      1{recurring_credit}
      Use this credit to pay for using <b>icebreakers</b> or for installing <b>virus</b> programs.
      """
