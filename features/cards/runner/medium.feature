Feature: Medium
  Background:
    Given the card is "Medium"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Medium"
    And the card has the subtypes "Virus"
    And the card has the faction "Anarch"
    And the card has a cost of 3
    And the card has an influence cost of 3
    And the card has a ram cost of 1
    And the card has the text
      """
      Whenever you make a successful run on R&D, place 1 virus counter on Medium.
      Each virus counter after the first on Medium allows you to access 1 additional card from R&D whenever you access cards from R&D.
      """
