Feature: Special Order
  Background:
    Given the card is "Special Order"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Special Order"
    And the card has the faction "Criminal"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has the text
      """
      Search your stack for an <b>icebreaker</b>, reveal it, and add it to your grip. Shuffle your stack.
      """
