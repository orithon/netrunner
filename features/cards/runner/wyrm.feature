Feature: Wyrm
  Background:
    Given the card is "Wyrm"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Program
    And the card has the name "Wyrm"
    And the card has the subtypes "Icebreaker, AI"
    And the card has the faction "Anarch"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has a ram cost of 1
    And the card has a strength of 1
    And the card has the text
      """
      3{credit}: Break ice subroutine on a piece of ice with 0 or less strength.
      1{credit}: Ice has -1 strength.
      1{credit}: +1 strength.
      """
