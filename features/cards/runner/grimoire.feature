Feature: Grimoire
  Background:
    Given the card is "Grimoire"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card is unique
    And the card has the name "Grimoire"
    And the card has the subtypes "Console"
    And the card has the faction "Anarch"
    And the card has a cost of 3
    And the card has an influence cost of 2
    And the card has the text
      """
      +2{ram}
      Whenever you install a <b>virus</b> program, place 1 virus counter on that program.
      Limit 1 <b>console</b> per player.
      """
