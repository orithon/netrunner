Feature: Wyldside
  Background:
    Given the card is "Wyldside"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a Resource
    And the card has the name "Wyldside"
    And the card has the subtypes "Location, Seedy"
    And the card has the faction "Anarch"
    And the card has a cost of 3
    And the card has an influence cost of 3
    And the card has the text
      """
      When your turn begins, draw 2 cards and lose {click}.
      """
