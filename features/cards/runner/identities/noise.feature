Feature: Noise: Hacker Extraordinaire
  Background:
    Given the card is "Noise: Hacker Extraordinaire"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a runner Identity
    And the card has the name "Noise: Hacker Extraordinaire"
    And the card has the display name "Noise"
    And the card has the flavour name "Hacker Extraordinaire"
    And the card has the subtypes "G-mod"
    And the card has the faction "Anarch"
    And the card has the deck size "45"
    And the card has the influence "15"
    And the card has the text
      """
      Whenever you install a <b>virus</b> program, the Corp trashes the top card of R&D.
      """

#  Scenario: Link for Noise
#    Given the runner has the deck "noise"
#    And the game has been initialized
#    Then the runner has 0 link

#  Scenario: Virus Program effect for Noise
#    Given the runner has the deck "noise"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner has "datasucker" in their hand
#    And the runner credit pool is set to 1
#    And the corporation discard pile is emptied
#    And "hedge fund" is removed from the corporation deck
#    And "hedge fund" is placed on the top of the corporation deck
#    When the runner installs "datasucker"
#    Then "hedge fund" is in the corporation discard pile

#  Scenario: Non-Virus Program effect for Noise
#    Given the runner has the deck "noise"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner has "djinn" in their hand
#    And the runner credit pool is set to 2
#    And the corporation discard file is emptied
#    And "hedge fund" is removed from the corporation deck
#    And "hedge fund" is placed on the top of the corporation deck
#    When the runner installs "djinn"
#    Then "hedge fund" is not in the corporation discard pile
