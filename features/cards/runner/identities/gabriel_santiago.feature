Feature: Gabriel Santiago: Consummate Professional
  Background:
    Given the card is "Gabriel Santiago: Consummate Professional"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a runner Identity
    And the card has the name "Gabriel Santiago: Consummate Professional"
    And the card has the display name "Gabriel Santiago"
    And the card has the flavour name "Consummate Professional"
    And the card has the subtypes "Cyborg"
    And the card has the faction "Criminal"
    And the card has the deck size "45"
    And the card has the influence "15"
    And the card has the text
      """
      The first time you make a successful run on HQ each turn, gain 2{credit}.
      """

#  Scenario: Link for Gabriel Santiago
#    Given the runner has the deck "gabriel santiago"
#    And the game has been initialized
#    Then the runner has 0 link

#  Scenario: Successful run on HQ effect for Gabriel Santiago
#    Given the runner has the deck "gabriel santiago"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner credit pool is set to 0
#    When the runner makes a run on the corporation's hand
#    Then the runner has 2 credits

#  Scenario: Unsuccessful run on HQ effect for Gabriel Santiago
#    Given the runner has the deck "gabriel santiago"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner credit pool is set to 0
#    And the corporation has "wall of static" defending their hand
#    When the runner makes a run on the corporation's hand
#    Then the runner has 0 credits

#  Scenario: Successful run on R&D effect for Gabriel Santiago
#    Given the runner has the deck "gabriel santiago"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner credit pool is set to 0
#    When the runner makes a run on the corporation's deck
#    Then the runner has 0 credits

#  Scenario: Successful run on Archives effect for Gabriel Santiago
#    Given the runner has the deck "gabriel santiago"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner credit pool is set to 0
#    When the runner makes a run on the corporation's discard pile
#    Then the runner has 0 credits

#  Scenario: Successful run on Remote effect for Gabriel Santiago
#    Given the runner has the deck "gabriel santiago"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner credit pool is set to 0
#    And the corporation has "pad campaign" installed in a remote server
#    When the runner makes a run on the corporation's remote server
#    Then the runner has 0 credits
