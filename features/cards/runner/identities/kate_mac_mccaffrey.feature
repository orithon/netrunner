Feature: Kate "Mac" McCaffrey: Digital Tinker
  Background:
    Given the card is "Kate "Mac" McCaffrey: Digital Tinker"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a runner Identity
    And the card has the name "Kate "Mac" McCaffrey: Digital Tinker"
    And the card has the display name "Kate "Mac" McCaffrey"
    And the card has the flavour name "Digital Tinker"
    And the card has the subtypes "Natural"
    And the card has the faction "Shaper"
    And the card has the deck size "45"
    And the card has the influence "15"
    And the card has the text
      """
      Lower the install cost of the first program or piece of hardware you install each turn by 1.
      """

  # Thing to consider for Kate: the discount can trigger each PLAYER turn.
  # That means if you have something that lets you do an install during the
  # corporation's turn, Kate still gets a discount!
#  Scenario: Link for Kate "Mac" McCaffrey
#    Given the runner has the deck "kate mac mccaffrey"
#    And the game has been initialized
#    Then the runner has 1 link

#  Scenario: Program Effect for Kate "Mac" McCaffrey
#    Given the runner has the deck "kate mac mccaffrey"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner has "magnum opus" in their hand
#    And the runner has "net shield" in their hand
#    And the runner credit pool is set to 6
#    When the runner installs "magnum opus"
#    And the runner installs "net shield"
#    Then the runner has 0 credits

#  Scenario: Hardware Effect for Kate "Mac" McCaffrey
#    Given the runner has the deck "kate mac mccaffrey"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner has "akamatsu mem chip" in their hand
#    And the runner has "the toolbox" in their hand
#    And the runner credit pool is set to 9
#    When the runner installs "akamatsu mem chip"
#    And the runner installs "the toolbox"
#    Then the runner has 0 credits

#  Scenario: Program then Hardware Effect for Kate "Mac" McCaffrey
#    Given the runner has the deck "kate mac mccaffrey"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner has "magnum opus" in their hand
#    And the runner has "akamatsu mem chip" in their hand
#    And the runner credit pool is set to 5
#    When the runner installs "magnum opus"
#    And the runner installs "akamatsu mem chip"
#    Then the runner has 0 credits

#  Scenario: Hardware then program Effect for Kate "Mac" McCaffrey
#    Given the runner has the deck "kate mac mccaffrey"
#    And the game has been initialized
#    And it is the runner's turn
#    And the runner has "the toolbox" in their hand
#    And the runner has "net shield" in their hand
#    And the runner credit pool is set to 10
#    When the runner installs "the toolbox"
#    And the runner installs "net shield"
#    Then the runner has 0 credits
