Feature: Sure Gamble
  Background:
    Given the card is "Sure Gamble"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is an Event
    And the card has the name "Sure Gamble"
    And the card has the faction "Neutral"
    And the card has a cost of 5
    And the card has an influence cost of 0
    And the card has the text
      """
      Gain 9{credit}.
      """
