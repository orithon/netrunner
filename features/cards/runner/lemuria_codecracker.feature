Feature: Lemuria Codecracker
  Background:
    Given the card is "Lemuria Codecracker"

  Scenario: Card Attributes
    Then the card is a Runner card
    And the card is a piece of Hardware
    And the card has the name "Lemuria Codecracker"
    And the card has the faction "Criminal"
    And the card has a cost of 1
    And the card has an influence cost of 1
    And the card has the text
      """
      {click}, 1{credit}: Expose 1 card. Use this ability only if you have made a successful run on HQ this turn.
      """
