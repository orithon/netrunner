Feature: Project Junebug
  Background:
    Given the card is "Project Junebug"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Asset
    And the card has the name "Project Junebug"
    And the card has the subtypes "Ambush, Research"
    And the card has the faction "Jinteki"
    And the card has a cost of 0
    And the card has an influence cost of 1
    And the card has a trash cost of 0
    And the card has the text
      """
      Project Junebug can be advanced.
      If you pay 1{credit} when the Runner accesses Project Junebug, do 2 net damage for each advancement token on Project Junebug.
      """
