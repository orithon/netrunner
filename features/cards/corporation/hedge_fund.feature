Feature: Hedge Fund
  Background:
    Given the card is "Hedge Fund"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Operation
    And the card has the name "Hedge Fund"
    And the card has the subtypes "Transaction"
    And the card has the faction "Neutral"
    And the card has a cost of 5
    And the card has an influence cost of 0
    And the card has the text
      """
      Gain 9{credit}
      """
