Feature: Neural Katana
  Background:
    Given the card is "Neural Katana"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Neural Katana"
    And the card has the subtypes "Sentry, AP"
    And the card has the faction "Jinteki"
    And the card has a cost of 4
    And the card has an influence cost of 2
    And the card has a strength of 3
    And the card has the text
      """
      {subroutine} Do 3 net damage.
      """
