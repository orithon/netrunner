Feature: Posted Bounty
  Background:
    Given the card is "Posted Bounty"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Agenda
    And the card has the name "Posted Bounty"
    And the card has the subtypes "Security"
    And the card has the faction "Weyland Consortium"
    And the card has a cost of 3
    And the card has a score of 1
    And the card has the text
      """
      When you score Posted Bounty, you may forfeit it to give the Runner 1 tag and take 1 bad publicity.
      """

