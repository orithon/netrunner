Feature: Precognition
  Background:
    Given the card is "Precognition"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Operation
    And the card has the name "Precognition"
    And the card has the faction "Jinteki"
    And the card has a cost of 0
    And the card has an influence cost of 3
    And the card has the text
      """
      Look at the top 5 cards of R&D and arrange them in any order.
      """
