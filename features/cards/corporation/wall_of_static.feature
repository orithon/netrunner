Feature: Wall of Static
  Background:
    Given the card is "Wall of Static"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Wall of Static"
    And the card has the subtypes "Barrier"
    And the card has the faction "Neutral"
    And the card has a cost of 3
    And the card has an influence cost of 0
    And the card has a strength of 3
    And the card has the text
      """
      {subroutine} End the run.
      """
