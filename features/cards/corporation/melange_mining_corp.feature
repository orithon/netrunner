Feature: Melange Mining Corp.
  Background:
    Given the card is "Melange Mining Corp."

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Asset
    And the card has the name "Melange Mining Corp."
    And the card has the faction "Neutral"
    And the card has a cost of 1
    And the card has an influence cost of 0
    And the card has a trash cost of 1
    And the card has the text
      """
      {click}, {click}, {click}: Gain 7{credit}.
      """
