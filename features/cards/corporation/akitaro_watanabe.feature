Feature: Akitaro Watanabe
  Background:
    Given the card is "Akitaro Watanabe"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Upgrade
    And the card is unique
    And the card has the name "Akitaro Watanabe"
    And the card has the subtypes "Sysop, Unorthodox"
    And the card has the faction "Jinteki"
    And the card has a cost of 1
    And the card has an influence cost of 2
    And the card has a trash cost of 3
    And the card has the text
      """
      The rez cost of ice protecting this server is lowered by 2.
      """

