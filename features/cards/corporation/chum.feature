Feature: Chum
  Background:
    Given the card is "Chum"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Chum"
    And the card has the subtypes "Code Gate"
    And the card has the faction "Jinteki"
    And the card has a cost of 1
    And the card has an influence cost of 1
    And the card has a strength of 4
    And the card has the text
      """
      {subroutine} The next piece of ice the Runner encounters during this run has +2 strength. Do 3 net damage unless the Runner breaks all subroutines on that piece of ice.
      """
