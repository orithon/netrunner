Feature: Braintrust
  Background:
    Given the card is "Braintrust"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Agenda
    And the card has the name "Braintrust"
    And the card has the subtypes "Research"
    And the card has the faction "Jinteki"
    And the card has a cost of 3
    And the card has a score of 2
    And the card has the text
      """
    When you score Braintrust, place 1 agenda counter on it for every 2 advancement tokens on it over 3.
    The rez cost of all ice is lowered by 1 for each agenda counter on Braintrust.
      """

