Feature: Priority Requisition
  Background:
    Given the card is "Priority Requisition"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Agenda
    And the card has the name "Priority Requisition"
    And the card has the subtypes "Security"
    And the card has the faction "Neutral"
    And the card has a cost of 5
    And the card has a score of 3
    And the card has the text
      """
      When you score Priority Requisition, you may rez a piece of ice ignoring all costs.
      """

