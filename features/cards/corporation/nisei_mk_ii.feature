Feature: Nisei MK II
  Background:
    Given the card is "Nisei MK II"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Agenda
    And the card has the name "Nisei MK II"
    And the card has the subtypes "Initiative"
    And the card has the faction "Jinteki"
    And the card has a cost of 4
    And the card has a score of 1
    And the card has the text
      """
      Place 1 agenda counter on Nisei MK II when you score it.
      <strong>Hosted agenda counter</strong>: End the run.
      """
