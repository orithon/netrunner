Feature: Zaibatsu Loyalty
  Background:
    Given the card is "Zaibatsu Loyalty"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Asset
    And the card has the name "Zaibatsu Loyalty"
    And the card has the faction "Jinteki"
    And the card has a cost of 0
    And the card has an influence cost of 1
    And the card has a trash cost of 4
    And the card has the text
      """
      If the Runner is about to expose a card, you may rez Zaibatsu Loyalty.
      1{credit} or {trash}: Prevent 1 card from being exposed.
      """
