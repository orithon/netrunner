Feature: Wall of Thorns
  Background:
    Given the card is "Wall of Thorns"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Wall of Thorns"
    And the card has the subtypes "Barrier, AP"
    And the card has the faction "Jinteki"
    And the card has a cost of 8
    And the card has an influence cost of 1
    And the card has a strength of 5
    And the card has the text
      """
      {subroutine} Do 2 net damage.
      {subroutine} End the run.
      """
