Feature: Enigma
  Background:
    Given the card is "Enigma"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Enigma"
    And the card has the subtypes "Code Gate"
    And the card has the faction "Neutral"
    And the card has a cost of 3
    And the card has an influence cost of 0
    And the card has a strength of 2
    And the card has the text
      """
      {subroutine} The Runner loses {click}, if able.
      {subroutine} End the run.
      """
