Feature: Snare!
  Background:
    Given the card is "Snare!"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Asset
    And the card has the name "Snare!"
    And the card has the subtypes "Ambush"
    And the card has the faction "Jinteki"
    And the card has a cost of 0
    And the card has an influence cost of 2
    And the card has a trash cost of 0
    And the card has the text
      """
      If Snare! is accessed from R&D, the Runner must reveal it.
      If you pay 4{credit} when the Runner accesses Snare!, do 3 net damage and give the Runner 1 tag. Ignore this effect if the Runner accesses Snare! from Archives.
      """
