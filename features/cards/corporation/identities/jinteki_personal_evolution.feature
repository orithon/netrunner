Feature: Jinteki: Personal Evolution
  Background:
    Given the card is "Jinteki: Personal Evolution"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is a corporation Identity
    And the card has the name "Jinteki: Personal Evolution"
    And the card has the display name "Jinteki"
    And the card has the flavour name "Personal Evolution"
    And the card has the subtypes "Megacorp"
    And the card has the faction "Jinteki"
    And the card has the deck size "45"
    And the card has the influence "15"
    And the card has the text
      """
      Whenever an agenda is scored or stolen, do 1 net damage.
      """

#  Scenario: Agenda scored effect for Jinteki
#    Given the corporation has the deck "jinteki personal evolution"
#    And the game has been initialized
#    And it is the corporation's turn
#    And the corporation has "nisei mk ii" installed in a remote server
#    And the "nisei mk ii" has 4 advancement tokens placed on it
#    When the "nisei mk ii" is scored
#    Then the runner takes 1 net damage

#  Scenario: Agenda stolen effect for Jinteki
#    Given the corporation has the deck "jinteki personal evolution"
#    And the game has been initialized
#    And it is the runner's turn
#    And the corporation has "nisei mk ii" installed in a remote server
#    When the runner makes a run on the corporation's remote server
#    And the runner steals the "nisei mk ii"
#    Then the runner takes 1 net damage
