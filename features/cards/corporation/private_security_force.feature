Feature: Private Security Force
  Background:
    Given the card is "Private Security Force"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Agenda
    And the card has the name "Private Security Force"
    And the card has the subtypes "Security"
    And the card has the faction "Neutral"
    And the card has a cost of 4
    And the card has a score of 2
    And the card has the text
      """
      If the Runner is tagged, Private Security Force gains: "{click}: Do 1 meat damage."
      """
