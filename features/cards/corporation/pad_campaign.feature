Feature: PAD Campaign
  Background:
    Given the card is "PAD Campaign"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Asset
    And the card has the name "PAD Campaign"
    And the card has the subtypes "Advertisement"
    And the card has the faction "Neutral"
    And the card has a cost of 2
    And the card has an influence cost of 0
    And the card has a trash cost of 4
    And the card has the text
      """
      Gain 1{credit} when your turn begins.
      """
