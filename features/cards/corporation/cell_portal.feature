Feature: Cell Portal
  Background:
    Given the card is "Cell Portal"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Cell Portal"
    And the card has the subtypes "Code Gate, Deflector"
    And the card has the faction "Jinteki"
    And the card has a cost of 5
    And the card has an influence cost of 2
    And the card has a strength of 7
    And the card has the text
      """
      {subroutine} The Runner approaches the outermost piece of ice protecting the attacked server. Derez Cell Portal.
      """
