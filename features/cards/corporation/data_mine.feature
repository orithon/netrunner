Feature: Data Mine
  Background:
    Given the card is "Data Mine"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Data Mine"
    And the card has the subtypes "Trap, AP"
    And the card has the faction "Jinteki"
    And the card has a cost of 0
    And the card has an influence cost of 2
    And the card has a strength of 2
    And the card has the text
      """
      {subroutine} Do 1 net damage. Trash Data Mine.
      """
