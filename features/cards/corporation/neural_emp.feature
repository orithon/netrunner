Feature: Neural EMP
  Background:
    Given the card is "Neural EMP"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Operation
    And the card has the name "Neural EMP"
    And the card has the subtypes "Gray Ops"
    And the card has the faction "Jinteki"
    And the card has a cost of 2
    And the card has an influence cost of 2
    And the card has the text
      """
      Play only if the Runner made a run during his or her last turn.
      Do 1 net damage.
      """
