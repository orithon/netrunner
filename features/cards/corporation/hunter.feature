Feature: Hunter
  Background:
    Given the card is "Hunter"

  Scenario: Card Attributes
    Then the card is a Corporation card
    And the card is an Ice
    And the card has the name "Hunter"
    And the card has the subtypes "Sentry, Tracer, Observer"
    And the card has the faction "Neutral"
    And the card has a cost of 1
    And the card has an influence cost of 0
    And the card has a strength of 4
    And the card has the text
      """
      {subroutine} <b>Trace<sup>3</sup></b> - If successful, give the Runner 1 tag.
      """
