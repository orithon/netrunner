# frozen_string_literal: true

After do
  SubscriptionManager.instance.clear
end
