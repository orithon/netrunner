# frozen_string_literal: true

Then(/^the runner identity is a Runner card$/) do
  expect(@runner.identity.role).to eq('Runner')
end

Then(/^the runner identity has the faction "([^"]*)"$/) do |faction|
  expect(@runner.identity.faction).to eq(faction)
end

Then(/^the runner identity has the name "(.*)"$/) do |name|
  expect(@runner.identity.name).to eq(name)
end

Then(/^the runner identity has the display name "(.*)"$/) do |display_name|
  expect(@runner.identity.display_name).to eq(display_name)
end

Then(/^the runner identity has the flavour name "(.*)"$/) do |flavour_name|
  expect(@runner.identity.flavour_name).to eq(flavour_name)
end

Then(/^the runner identity has the subtypes "([^"]*)"$/) do |subtypes_string|
  subtypes = subtypes_string.split(',')
  expect(@runner.identity.subtypes).to match_array(subtypes)
end

Then(/^the runner identity has the text "([^"]*)"$/) do |text|
  expect(@runner.identity.text).to eq(text)
end

Then(/^the runner identity has the flavour text "(.*)"$/) do |flavour_text|
  expect(@runner.identity.flavour_text).to eq(flavour_text)
end

Then(/^the runner identity has the link "(\d+)"$/) do |link|
  expect(@runner.identity.link).to eq(link.to_i)
end

Then(/^the runner identity has the influence "(\d+)"$/) do |influence|
  expect(@runner.identity.influence).to eq(influence.to_i)
end

Then(/^the runner identity has the deck size "(\d+)"$/) do |deck_size|
  expect(@runner.identity.deck_size).to eq(deck_size.to_i)
end

Then(/^the runner has (\d+) link$/) do |link|
  expect(@runner.link).to eq(link.to_i)
end

Given(/^it is the runner's turn$/) do
  pending
end

Given(/^the runner has "([^"]*)" in their hand$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^the runner credit pool is set to (\d+)$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^the runner installs "([^"]*)"$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^the corporation discard pile is emptied$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^"([^"]*)" is removed from the corporation deck$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^"([^"]*)" is placed on the top of the corporation deck$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^"([^"]*)" is in the corporation discard pile$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^the corporation discard file is emptied$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^"([^"]*)" is not in the corporation discard pile$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^the runner makes a run on the corporation's hand$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^the corporation has "([^"]*)" defending their hand$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^the runner makes a run on the corporation's deck$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^the runner makes a run on the corporation's discard pile$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^the corporation has "([^"]*)" installed in a remote server$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^the runner makes a run on the corporation's remote server$/) do
  pending # Write code here that turns the phrase above into concrete actions
end
