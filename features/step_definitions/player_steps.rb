# frozen_string_literal: true

Given(/^the players have been initialized$/) do
  step 'the corporation has been initialized'
  step 'the runner has been initialized'
  @players ||= {
    corporation: @corporation,
    runner: @runner
  }
end

Given(/^the corporation has been initialized$/) do
  step 'the corporation has the deck "jinteki personal evolution"' if @corporation_deck.nil?
  @corporation ||= Corporation.new(@corporation_deck)
end

Given(/^the runner has been initialized$/) do
  step 'the runner has the deck "kate mac mccaffrey"' if @runner_deck.nil?
  @runner ||= Runner.new(@runner_deck)
end

Given(/^the (.*) has been given (\d+) credits$/) do |player_key, amount|
  # First we clear out their existing credits
  player(player_key).lose_credits(player(player_key).credits)
  player(player_key).gain_credits(amount.to_i)
end

Given(/^the (.*) has been given (\d+) clicks/) do |player_key, amount|
  # First we clear out their existing clicks
  player(player_key).lose_clicks(player(player_key).clicks)
  player(player_key).gain_clicks(amount.to_i)
end

Then(/^the (.*) has (\d+) credits$/) do |player_key, amount|
  # This includes only those credits that are in the player's main credit pool
  expect(player(player_key).credits).to eq(amount.to_i)
end

Then(/^the (.*) has (\d+) clicks$/) do |player_key, amount|
  expect(player(player_key).clicks).to eq(amount.to_i)
end

Then(/^the (.*) deck has been shuffled$/) do |player_key|
  expect(player(player_key).deck.shuffled?).to be true
end

Then(/^the (.*) has (\d+) cards in their hand$/) do |player_key, amount|
  expect(player(player_key).hand.count).to eq(amount.to_i)
end

def player(player_key)
  @players[player_key.intern]
end
