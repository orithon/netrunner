# frozen_string_literal: true

require 'netrunner/piece/card/identity'
require 'netrunner/piece/card/agenda'
require 'netrunner/piece/card/asset'
require 'netrunner/piece/card/ice'
require 'netrunner/piece/card/operation'
require 'netrunner/piece/card/upgrade'

require 'netrunner/piece/card/event'
require 'netrunner/piece/card/hardware'
require 'netrunner/piece/card/program'
require 'netrunner/piece/card/resource'

Given(/^the card is "(.*)"$/) do |card_name|
  @card = Object.const_get(Card.class_name_from_card_name(card_name)).new
end

Then(/^the card is a (.*) card$/) do |card_role|
  expect(@card.role).to eq(card_role)
end

Then(/^the card is a corporation Identity$/) do
  expect(@card.type).to eq('Identity')
  expect(@card).to be_a(Identity::Corporation)
end

Then(/^the card is a runner Identity$/) do
  expect(@card.type).to eq('Identity')
  expect(@card).to be_a(Identity::Runner)
end

Then(/^the card is an Agenda$/) do
  expect(@card.type).to eq('Agenda')
  expect(@card).to be_a(Agenda)
end

Then(/^the card is an Asset$/) do
  expect(@card.type).to eq('Asset')
  expect(@card).to be_a(Asset)
end

Then(/^the card is an Ice$/) do
  expect(@card.type).to eq('Ice')
  expect(@card).to be_a(Ice)
end

Then(/^the card is an Operation$/) do
  expect(@card.type).to eq('Operation')
  expect(@card).to be_a(Operation)
end

Then(/^the card is an Upgrade$/) do
  expect(@card.type).to eq('Upgrade')
  expect(@card).to be_a(Upgrade)
end

Then(/^the card is an Event$/) do
  expect(@card.type).to eq('Event')
  expect(@card).to be_a(Event)
end

Then(/^the card is a piece of Hardware$/) do
  expect(@card.type).to eq('Hardware')
  expect(@card).to be_a(Hardware)
end

Then(/^the card is a Program$/) do
  expect(@card.type).to eq('Program')
  expect(@card).to be_a(Program)
end

Then(/^the card is a Resource$/) do
  expect(@card.type).to eq('Resource')
  expect(@card).to be_a(Resource)
end

Then(/^the card is unique$/) do
  expect(@card).to be_unique
end

Then(/^the card has the name "(.*)"$/) do |name|
  expect(@card.name).to eq(name)
end

Then(/^the card has the display name "(.*)"$/) do |display_name|
  expect(@card.display_name).to eq(display_name)
end

Then(/^the card has the flavour name "(.*)"$/) do |flavour_name|
  expect(@card.flavour_name).to eq(flavour_name)
end

Then(/^the card has the subtypes "([^"]*)"$/) do |subtypes_string|
  subtypes = subtypes_string.split(',').map(&:strip)
  expect(@card.subtypes).to eq(subtypes)
end

Then(/^the card has the faction "([^"]*)"$/) do |faction|
  expect(@card.faction).to eq(faction)
end

Then(/^the card has the influence "([^"]*)"$/) do |influence|
  expect(@card.influence).to eq(influence.to_i)
end

Then(/^the card has the deck size "([^"]*)"$/) do |deck_size|
  expect(@card.deck_size).to eq(deck_size.to_i)
end

Then(/^the card has a cost of (\d+)$/) do |cost|
  expect(@card.cost).to eq(cost.to_i)
end

Then(/^the card has a score of (\d+)$/) do |score|
  expect(@card.score).to eq(score.to_i)
end

Then(/^the card has an influence cost of (\d+)$/) do |influence_cost|
  expect(@card.influence_cost).to eq(influence_cost.to_i)
end

Then(/^the card has a trash cost of (\d+)$/) do |trash_cost|
  expect(@card.trash_cost).to eq(trash_cost.to_i)
end

Then(/^the card has a ram cost of (\d+)$/) do |ram_cost|
  expect(@card.ram_cost).to eq(ram_cost.to_i)
end

Then(/^the card has a strength of (\d+)$/) do |strength|
  expect(@card.strength).to eq(strength.to_i)
end

Then(/^the card has the text$/) do |text|
  expect(@card.text).to eq(text)
end
