# frozen_string_literal: true

require 'netrunner/messaging/subscription_manager'
require 'netrunner/messaging/message'
require 'netrunner/messaging/messenger'
require 'netrunner/messaging/collectable'

Given(/^the subscriptions list is empty$/) do
  SubscriptionManager.instance.clear
end

Given(/^there are (\d+) collectable objects$/) do |count|
  count = count.to_i
  @collectables = []
  (1..count).each { @collectables << CollectableObject.new }
end

Given(/^there is an event "([^"]*)"$/) do |event|
  @event = Message.new(type: event, payload: nil)
end

When(/^a collection message for the objects is sent$/) do
  @message = Message.new(type: CollectableObject::AGGREGATION_EVENT, payload: [])
  SubscriptionManager.instance.deliver(@message)
end

Then(/^the message payload contains the collectable objects$/) do
  expect(@message.payload).to contain_exactly(*@collectables)
end

class CollectableObject
  include Messenger
  include Collectable

  AGGREGATION_EVENT = 'AGGREGATE_COLLECTABLE_OBJECT'

  def initialize
    subscribe(message: AGGREGATION_EVENT, action: :collect)
  end
end
