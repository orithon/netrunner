# frozen_string_literal: true

require 'netrunner/game'
require 'netrunner/player/corporation'
require 'netrunner/player/runner'
require 'netrunner/phases/phase'
require 'netrunner/zone'
require 'netrunner/event'
require 'netrunner/piece/credit'
require 'netrunner/messaging/targetted_payload'

def parse_the_deck(deck_name)
  step %(there is a deck file for "#{deck_name}")
  step 'the deck file is parsed'
  step 'the deck is generated successfully'
  @deck
end

Given(/^the runner has the deck "([^"]*)"$/) do |deck_name|
  @runner_deck ||= parse_the_deck(deck_name)
end

Given(/^the corporation has the deck "([^"]*)"$/) do |deck_name|
  @corporation_deck ||= parse_the_deck(deck_name)
end

Given(/^the game has been initialized$/) do
  # What needs to happen here?  I suppose we create an actual Game object,
  # with the two players (Runner and Corporation) as arguments.
  # Should probably write some cukes for players first...  Make sure that they
  # can have a deck, a credit pool, play area, etc.
  step 'the players have been initialized'
  @game ||= Game.new(@players)
  expect(@game.corporation).to be(@corporation)
  expect(@game.runner).to be(@runner)
end

Given(/^the (.*) has a (.*) in their hand$/) do |player_key, card_name|
  @card_in_hand = instantiate_card(card_name)
  player(player_key).deck.add_to_deck(@card_in_hand)
  player(player_key).draw_card
end

Given(/^the phase is ([^"]*)$/) do |phase_name|
  # Move the game phase along until we reach the desired phase.
  # WARNING: This can cause an infinite loop if the given phase doesn't exist!
  @game.increment_phase(true) until @game.phase.name == phase_name
end

Given(/^the game is processed$/) do
  process
end

Given(/^the (.*) has a (.*) in play$/) do |player_key, card_name|
  card = instantiate_card(card_name)
  player(player_key).deck.add_to_deck(card)
  player(player_key).draw_card
  card.play
end

When(/^the (.*) chooses to (.*)$/) do |player_key, action_name|
  # 1. Grabs the symbol for the chosen action
  # 2. Ensures that the chosen action is available to the player
  # 3. Tell the game to process the chosen action
  chosen_action = events[player_key][action_name]
  expect(@available_actions).to include(chosen_action)
  process(chosen_action)
end

When(/^the (.*) plays the card in their hand/) do |_player_key|
  expect(@available_actions).to include(@card_in_hand.address)
  process(@card_in_hand.address)
end

When(/^the (.*) installs the card in their hand$/) do |_player_key|
  expect(@available_actions).to include(@card_in_hand.address)
  process(@card_in_hand.address)
end

When(/^the (.*) is presented with the credits they could pay with/) do |_player_key|
  expect(@available_actions_objects.count).to be > 0
  expect(@available_actions_objects).to all(be_a(Credit))
end

When(/^the credits the (.*) is presented with includes (\d+) recurring credit$/) do |player_key, amount|
  step "the #{player_key} is presented with the credits they could pay with"
  unspent_recurring_credits = @available_actions_objects.select { |credit| credit.is_a?(Credit::Recurring) && !credit.spent? }
  expect(unspent_recurring_credits.count).to eq(amount.to_i)
end

When(/^the runner spends the credit on Cyberfeeder$/) do
  cyberfeeder_credits = @available_actions_objects.select { |object| object.is_a?(Cyberfeeder::CyberfeederCredit) }
  expect(cyberfeeder_credits.count).to eq 1
  process(cyberfeeder_credits.first.address)
  require 'byebug'; byebug
end

Then(/^the (.*) identity is in play$/) do |player_key|
  expect(player(player_key).identity.in_play?).to be true
end

Then(/^the card is now in archives$/) do
  expect(@card_in_hand.zone).to eq(Zone::Corporation::DISCARD)
end

Then(/^the current phase is ([^"]*)$/) do |phase_name|
  expect(@game.phase.name).to eq(phase_name)
end

Then(/^the (.*) has (\d+) recurring credit that has not been spent$/) do |player_key, amount|
  aggregation_event = if player_key == 'runner'
                        Event::Aggregation::RUNNER_CREDITS_AGGREGATION
                      elsif player_key == 'corporation'
                        Event::Aggregation::CORPORATION_CREDITS_AGGREGATION
                      end

  payload = TargettedPayload.new(target: nil)
  SubscriptionManager.instance.deliver(Message.new(type: aggregation_event, payload: payload))
  unspent_recurring_credits = payload.select { |credit| credit.is_a?(Credit::Recurring) && !credit.spent? }
  expect(unspent_recurring_credits.count).to eq(amount.to_i)
end

Then(/^the runner has (\d+) credits? for installing virus programs$/) do |amount|
  # Bare minimum for a virus program
  class VirusProgram < RunnerCard
    include Program

    def initialize_attributes
      super
      @subtypes = [Subtype::VIRUS]
    end
  end

  payload = TargettedPayload.new(target: VirusProgram.new)
  SubscriptionManager.instance.deliver(Message.new(type: Event::Aggregation::RUNNER_CREDITS_AGGREGATION, payload: payload))
  expect(payload.count).to eq(amount.to_i)
end

def process(action = nil)
  @available_actions = @game.process(action)
  @available_actions_objects = @game.fetch_objects(@available_actions)
end

def instantiate_card(card_name)
  card_class_name = Card.class_name_from_card_name(card_name)
  Object.const_get(card_class_name).new
end

def player(player_key)
  @players[player_key.intern]
end

def events
  @events ||= {
    'corporation' => corporation_events,
    'runner' => runner_events
  }
end

def corporation_events
  @corporation_events ||= {
    'draw a card' => Event::CORP_DRAW_ACTION,
    'gain a credit' => Event::CORP_GAIN_CREDIT_ACTION,
    'install a card' => Event::CORP_INSTALL_ACTION,
    'play an operation' => Event::CORP_PLAY_OPERATION_ACTION,
    'advance a card' => Event::CORP_ADVANCE_ACTION,
    'purge virus counters' => Event::CORP_PURGE_ACTION,
    'trash a resource' => Event::CORP_TRASH_RESOURCE_ACTION
  }
end

def runner_events
  @runner_events ||= {
    'draw a card' => Event::RUNNER_DRAW_ACTION,
    'gain a credit' => Event::RUNNER_GAIN_CREDIT_ACTION,
    'install a card' => Event::RUNNER_INSTALL_ACTION,
    'play an event' => Event::RUNNER_PLAY_EVENT_ACTION,
    'remove a tag' => Event::RUNNER_REMOVE_TAG_ACTION,
    'make a run' => Event::RUNNER_RUN_ACTION
  }
end
