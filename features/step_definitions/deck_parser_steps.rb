# frozen_string_literal: true

require 'netrunner/parsers/deck_parser'
require 'netrunner/piece/deck'

Given(/^there is a deck file for "([^"]*)"$/) do |deck_name|
  expect do
    @deck_data = File.read("features/deck_lists/#{deck_name}.deck")
  end.to_not raise_error
  expect(@deck_data).to_not be nil
end

When(/^the deck file is parsed$/) do
  @parser = DeckParser.new(@deck_data)

  begin
    @deck = @parser.call
  rescue StandardError => e
    @error = e
  end
end

Then(/^the deck is generated successfully$/) do
  expect(@error).to be nil
  expect(@deck).to_not be nil
  expect(@deck).to be_a(Deck)
end

Then(/^the deck's identity card is "(.*)"$/) do |identity_name|
  expect(@deck.identity.name).to eq(identity_name)
end

Then(/^the deck contains (\d+) (.*) cards?$/) do |amount, card_name|
  cards = @deck.cards.select { |card| card.name == card_name }
  expect(cards.count).to be(amount.to_i)
end

Then(/^the deck parser complains that no identity was given$/) do
  expect(@error).to be_a(DeckParser::NoSuchIdentityError)
end

Then(/^the deck parser complains that more than one identity was given$/) do
  expect(@error).to be_a(Deck::MoreThanOneIdentityError)
end

Then(/^the deck parser complains that corporation cards have been included in a runner deck$/) do
  expect(@error).to be_a(Deck::RunnerDeckContainsCorporationCardsError)
end

Then(/^the deck parser complains that runner cards have been included in a corporation deck$/) do
  expect(@error).to be_a(Deck::CorporationDeckContainsRunnerCardsError)
end

Then(/^the deck parser complains that there are not enough cards in the deck$/) do
  expect(@error).to be_a(Deck::TooFewCardsError)
end

Then(/^the deck parser complains that there are too many copies of a card$/) do
  expect(@error).to be_a(Deck::TooManyCardCopiesError)
end

Then(/^the deck parser complains that there are too many out of faction cards$/) do
  expect(@error).to be_a(Deck::TooManyOutOfFactionCardsError)
end

Then(/^the deck parser complains that there are faction specific cards the identity cannot use$/) do
  expect(@error).to be_a(Deck::FactionSpecificCardsMismatchError)
end

Then(/^the deck parser complains that there are too few agenda points$/) do
  expect(@error).to be_a(Deck::TooFewAgendaPointsError)
end

Then(/^the deck parser complains that there are too many agenda points$/) do
  expect(@error).to be_a(Deck::TooManyAgendaPointsError)
end

Then(/^the deck parser complains that the deck file was empty$/) do
  expect(@error).to be_a(DeckParser::EmptyFileError)
end

Then(/^the deck parser complains that the cards do not exist$/) do
  expect(@error).to be_a(DeckParser::InvalidCardsError)
end
