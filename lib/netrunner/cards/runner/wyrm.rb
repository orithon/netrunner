# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Wyrm < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Wyrm'
    @subtypes = [Subtype::ICEBREAKER, Subtype::AI]
    @faction = Faction::Runner::ANARCH
    @cost = 1
    @influence_cost = 2
    @ram_cost = 1
    @strength = 1
    @text = %(3{credit}: Break ice subroutine on a piece of ice with 0 or less strength.
1{credit}: Ice has -1 strength.
1{credit}: +1 strength.)
    @flavour_text = 'Fire and ichor...'
  end
end
