# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'
require 'netrunner/subtype'

class DemolitionRun < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Demolition Run'
    @subtypes = [Subtype::RUN, Subtype::SABOTAGE]
    @faction = Faction::Runner::ANARCH
    @cost = 2
    @influence_cost = 2
    @text = 'Make a run on HQ or R&D. You may trash, at no cost, any cards you access (even if the cards cannot normally be trashed).'
    @flavour_text = 'You ever set something on fire just to watch it burn?'
  end
end
