# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Yog0 < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Yog.0'
    @subtypes = [Subtype::ICEBREAKER, Subtype::DECODER]
    @faction = Faction::Runner::ANARCH
    @cost = 5
    @influence_cost = 1
    @ram_cost = 1
    @strength = 3
    @text = '0{credit}: Break <b>code gate</b> subroutine.'
    @flavour_text = "The Yog.0 database is a crowdsourced compilation of sniffed, spoofed, and logged passkeys. If the key to the gate is in the database, you're in. If it's not, change the gate!"
  end
end
