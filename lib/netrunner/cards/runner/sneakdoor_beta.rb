# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/faction'

class SneakdoorBeta < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Sneakdoor Beta'
    @faction = Faction::Runner::CRIMINAL
    @cost = 4
    @influence_cost = 3
    @ram_cost = 2
    @text = '{click}: Make a run on Archives. If successful, instead treat it as a successful run on HQ.'
    @flavour_text = %("The code isn't important. It's where the code takes you that is important." - g00ru)
  end
end
