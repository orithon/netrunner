# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/unique'
require 'netrunner/faction'
require 'netrunner/subtype'

class TheToolbox < RunnerCard
  include Hardware
  include Unique

  def initialize_attributes
    super

    @name = 'The Toolbox'
    @subtypes = [Subtype::CONSOLE]
    @faction = Faction::Runner::SHAPER
    @cost = 9
    @influence_cost = 2
    @text = %(+2{ram} +2{link}
2{recurring_credit}
Use these credits to pay for using <b>icebreakers</b>.
Limit 1 <b>console</b> per player.)
  end
end
