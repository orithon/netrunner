# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/unique'
require 'netrunner/faction'
require 'netrunner/subtype'

class AesopsPawnshop < RunnerCard
  include Resource
  include Unique

  def initialize_attributes
    super

    @name = "Aesop's Pawnshop"
    @subtypes = [Subtype::LOCATION, Subtype::CONNECTION]
    @faction = Faction::Runner::SHAPER
    @cost = 1
    @influence_cost = 2
    @text = 'When your turn begins, you may trash another of your installed cards to gain 3{credit}.'
    @flavour_text = "You didn't mention Aesop's arm unless you wanted an earful. Sometimes he talked about it in such a way that you wondered why he didn't laser off his other arm as well."
  end
end
