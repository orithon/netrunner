# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/subtype'
require 'netrunner/faction'

class Medium < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Medium'
    @subtypes = [Subtype::VIRUS]
    @faction = Faction::Runner::ANARCH
    @cost = 3
    @influence_cost = 3
    @ram_cost = 1
    @text = %(Whenever you make a successful run on R&D, place 1 virus counter on Medium.
Each virus counter after the first on Medium allows you to access 1 additional card from R&D whenever you access cards from R&D.)
    @flavour_text = "It looked like random packet loss. It wasn't."
  end
end
