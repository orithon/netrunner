# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Crypsis < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Crypsis'
    @subtypes = [Subtype::ICEBREAKER, Subtype::AI, Subtype::VIRUS]
    @faction = Faction::Runner::NEUTRAL
    @cost = 5
    @influence_cost = 0
    @ram_cost = 1
    @strength = 0
    @text = %(1{credit}: Break ice subroutine.
1{credit}: +1 strength.
{click}: Place 1 virus counter on Crypsis.
When an encounter with a piece of ice in which you used Crypsis to break a subroutine ends, remove 1 hosted virus counter or trash Crypsis.)
  end
end
