# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/faction'

class LemuriaCodecracker < RunnerCard
  include Hardware

  def initialize_attributes
    super

    @name = 'Lemuria Codecracker'
    @faction = Faction::Runner::CRIMINAL
    @cost = 1
    @influence_cost = 1
    @text = '{click}, 1{credit}: Expose 1 card. Use this ability only if you have made a successful run on HQ this turn.'
    @flavour_text = %("A little preparation goes a long way."
\t- Gabriel Santiago)
  end
end
