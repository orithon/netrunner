# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class ArmitageCodebusting < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Armitage Codebusting'
    @subtypes = [Subtype::JOB]
    @faction = Faction::Runner::NEUTRAL
    @cost = 1
    @influence_cost = 0
    @text = %(Place 12{credit} from the bank on Armitage Codebusting when it is installed. When there are no credits left on Armitage Codebusting, trash it.
{click}: Take 2{credit} from Armitage Codebusting.)
    @flavour_text = 'Drudge work, but it pays the bills.'
  end
end
