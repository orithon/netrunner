# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'
require 'netrunner/subtype'

class AccountSiphon < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Account Siphon'
    @subtypes = [Subtype::RUN, Subtype::SABOTAGE]
    @faction = Faction::Runner::CRIMINAL
    @cost = 0
    @influence_cost = 4
    @text = 'Make a run on HQ. If successful, instead of accessing cards you may force the Corp to lose up to 5{credit}, then you gain 2{credit} for each credit lost and take 2 tags.'
  end
end
