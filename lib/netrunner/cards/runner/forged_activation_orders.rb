# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'
require 'netrunner/subtype'

class ForgedActivationOrders < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Forged Activation Orders'
    @subtypes = [Subtype::SABOTAGE]
    @faction = Faction::Runner::CRIMINAL
    @cost = 1
    @influence_cost = 2
    @text = 'Choose an unrezzed piece of ice. The Corp must either rez that ice or trash it.'
    @flavour_text = 'As the hysteria in the room climbed higher up the corporate chain, an uneasy feeling of joblessness began to sink in on the lower rungs.'
  end
end
