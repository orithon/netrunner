# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class AccessToGlobalsec < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Access to Globalsec'
    @subtypes = [Subtype::LINK]
    @faction = Faction::Runner::NEUTRAL
    @cost = 1
    @influence_cost = 0
    @text = '+1{link}'
    @flavour_text = 'He flicked the display population to high, and was surrounded by a circle of floating holos. The ping-back was strong, the clearance level blue-one. Now to find the perfect place for a relay...'
  end
end
