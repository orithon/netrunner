# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Aurora < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Aurora'
    @subtypes = [Subtype::ICEBREAKER, Subtype::FRACTER]
    @faction = Faction::Runner::CRIMINAL
    @cost = 3
    @influence_cost = 1
    @ram_cost = 1
    @strength = 1
    @text = %(2{credit}: Break <b>barrier</b> subroutine.
2{credit}: +3 strength.)
  end
end
