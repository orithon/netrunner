# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/subtype'
require 'netrunner/faction'

class Datasucker < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Datasucker'
    @subtypes = [Subtype::VIRUS]
    @faction = Faction::Runner::ANARCH
    @cost = 1
    @influence_cost = 1
    @ram_cost = 1
    @text = %(Whenever you make a successful run on a central server, place 1 virus counter on Datasucker.
<b>Hosted virus counter</b>: Rezzed piece of ice currently being encountered has -1 strength until the end of the encounter.)
  end
end
