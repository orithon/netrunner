# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'

class Diesel < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Diesel'
    @faction = Faction::Runner::SHAPER
    @cost = 0
    @influence_cost = 2
    @text = 'Draw 3 cards.'
    @flavour_text = 'Diesel gives you flames.'
  end
end
