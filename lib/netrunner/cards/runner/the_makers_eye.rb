# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/subtype'
require 'netrunner/faction'

class TheMakersEye < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = "The Maker's Eye"
    @subtypes = [Subtype::RUN]
    @faction = Faction::Runner::SHAPER
    @cost = 2
    @influence_cost = 2
    @text = 'Make a run on R&D. If successful, access 2 additional cards from R&D.'
    @flavour_text = %("Some of the professionals have good instincts, but they can't see beyond the data. They can't see the matrix." - Ele "Smoke" Scovak)
  end
end
