# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/faction'

class NetShield < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Net Shield'
    @faction = Faction::Runner::SHAPER
    @cost = 2
    @influence_cost = 1
    @ram_cost = 1
    @text = '1{credit}: Prevent the first net damage this turn.'
    @flavour_text = 'Sucks energy like a Martian terra-bot, but it keeps you focused.'
  end
end
