# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/subtype'
require 'netrunner/faction'

class Djinn < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Djinn'
    @subtypes = [Subtype::DAEMON]
    @faction = Faction::Runner::ANARCH
    @cost = 2
    @influence_cost = 2
    @ram_cost = 1
    @text = %(Djinn can host up to 3{ram} of non-<b>icebreaker</b> programs.
The memory costs of hosted programs do not count against your memory limit.
{click}, 1{credit}: Search your stack for a <b>virus</b> program, reveal it, and add it to your grip. Shuffle your stack.)
  end
end
