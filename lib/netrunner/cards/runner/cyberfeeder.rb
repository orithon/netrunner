# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/piece/credit'
require 'netrunner/faction'
require 'netrunner/subtype'
require 'netrunner/event'
require 'netrunner/zone'

# Representation of the Cyberfeeder card.
class Cyberfeeder < RunnerCard
  include Hardware

  def initialize_attributes
    super

    @name = 'Cyberfeeder'
    @subtypes = [Subtype::CHIP]
    @faction = Faction::Runner::ANARCH
    @cost = 2
    @influence_cost = 1
    @text = %(1{recurring_credit}
Use this credit to pay for using <b>icebreakers</b> or for installing <b>virus</b> programs.)
    @flavour_text = 'I feel almost naked without it.'
  end

  def zone_subscriptions
    super.tap do |subs|
      subs[Zone::Runner::PLAY_AREA] << { message: Event::BEGIN_CORP_TURN, action: :refresh_recurring_credit }
    end
  end

  def play
    super

    @recurring_credit = CyberfeederCredit.new(self)
    refresh_recurring_credit
  end

  def refresh_recurring_credit
    @recurring_credit.refresh
  end

  # The recurring credit that lives on Cyberfeeder.
  class CyberfeederCredit < Credit
    include Credit::Recurring

    attr_reader :hosted_card

    def initialize(hosted_card)
      super()
      @hosted_card = hosted_card
    end

    def refresh
      move(hosted_card.address)
    end

    def zone_subscriptions
      super.tap do |subs|
        subs[hosted_card.address] << { message: Event::Aggregation::RUNNER_CREDITS_AGGREGATION, action: :collect }
      end
    end

    def collect(message)
      target = message.payload.target
      super if target.nil? || (target.is_a?(Program) && target.subtypes.include?(Subtype::VIRUS))
    end
  end
end
