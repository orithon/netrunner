# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class BankJob < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Bank Job'
    @subtypes = [Subtype::JOB]
    @faction = Faction::Runner::CRIMINAL
    @cost = 1
    @influence_cost = 2
    @text = %(Place 8{credit} from the bank on Bank Job when it is installed. When there are no credits left on Bank Job, trash it.
Whenever you make a successful run on a remote server, instead of accessing cards you may take any number of credits from Bank Job.)
  end
end
