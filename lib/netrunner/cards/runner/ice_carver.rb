# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class IceCarver < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Ice Carver'
    @subtypes = [Subtype::VIRTUAL]
    @faction = Faction::Runner::ANARCH
    @cost = 3
    @influence_cost = 3
    @text = 'All ice is encountered with its strength lowered by 1.'
    @flavour_text = "In the public consciousness, there's a hard line between corp and runner. In the real world, things are a little more porous. The corps need the best hackers to run their networks, and some of the best hackers are ex-runners who like the idea of a regular paycheck. But sometimes things run the other way, and someone on the inside makes something like this."
  end
end
