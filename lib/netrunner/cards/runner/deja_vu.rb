# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'

class DejaVu < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Déjà Vu'
    @faction = Faction::Runner::ANARCH
    @cost = 2
    @influence_cost = 2
    @text = 'Add 1 card (or up to 2 <b>virus</b> cards) from your heap to your grip.'
    @flavour_text = 'Anything worth doing is worth doing twice.'
  end
end
