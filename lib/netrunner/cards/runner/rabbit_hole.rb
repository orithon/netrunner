# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/faction'
require 'netrunner/subtype'

class RabbitHole < RunnerCard
  include Hardware

  def initialize_attributes
    super

    @name = 'Rabbit Hole'
    @subtypes = [Subtype::LINK]
    @faction = Faction::Runner::SHAPER
    @cost = 2
    @influence_cost = 1
    @text = %(+1{link}
When Rabbit Hole is installed, you may search your stack for another copy of Rabbit Hole and install it by paying its install cost. Shuffle your stack.)
    @flavour_text = "It's not endless, it just feels that way."
  end
end
