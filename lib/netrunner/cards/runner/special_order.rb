# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'

class SpecialOrder < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Special Order'
    @faction = Faction::Runner::CRIMINAL
    @cost = 1
    @influence_cost = 2
    @text = 'Search your stack for an <b>icebreaker</b>, reveal it, and add it to your grip. Shuffle your stack.'
    @flavour_text = 'Feverishly tracking its frustratingly slow progress across the Pacific, the package finally shows up hours later...'
  end
end
