# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class GordianBlade < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Gordian Blade'
    @subtypes = [Subtype::ICEBREAKER, Subtype::DECODER]
    @faction = Faction::Runner::SHAPER
    @cost = 4
    @influence_cost = 3
    @ram_cost = 1
    @strength = 2
    @text = %(1{credit}: Break <b>code gate</b> subroutine.
1{credit}: +1 strength for the remainder of this run.)
    @flavour_text = 'It can slice through the thickest knots of data.'
  end
end
