# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/unique'
require 'netrunner/faction'
require 'netrunner/subtype'

class Wyldside < RunnerCard
  include Resource
  include Unique

  def initialize_attributes
    super

    @name = 'Wyldside'
    @subtypes = [Subtype::LOCATION, Subtype::SEEDY]
    @faction = Faction::Runner::ANARCH
    @cost = 3
    @influence_cost = 3
    @text = 'When your turn begins, draw 2 cards and lose {click}.'
    @flavour_text = %("Best place to go when you want to get your mind out of the gutter and take it inside"
\t- Ji "Noise" Reilly)
  end
end
