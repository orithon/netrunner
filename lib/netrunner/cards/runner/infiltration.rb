# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'

class Infiltration < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Infiltration'
    @faction = Faction::Runner::NEUTRAL
    @cost = 0
    @influence_cost = 0
    @text = 'Gain 2{credit} or expose 1 card.'
    @flavour_text = %("Bring back any memories, Monica?"
\t- John "Animal" McEvoy)
  end
end
