# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Corroder < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Corroder'
    @subtypes = [Subtype::ICEBREAKER, Subtype::FRACTER]
    @faction = Faction::Runner::ANARCH
    @cost = 2
    @influence_cost = 2
    @ram_cost = 1
    @strength = 2
    @text = %(1{credit}: Break <b>barrier</b> subroutine.
1{credit}: +1 strength.)
    @flavour_text = %("If at first you don't succeed, boost its strength and try again." - g00ru)
  end
end
