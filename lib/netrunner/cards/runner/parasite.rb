# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/subtype'
require 'netrunner/faction'

class Parasite < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Parasite'
    @subtypes = [Subtype::VIRUS]
    @faction = Faction::Runner::ANARCH
    @cost = 2
    @influence_cost = 2
    @ram_cost = 1
    @text = %(Install Parasite only on a rezzed piece of ice.
Host ice has -1 strength for each virus counter on Parasite and is trashed if its strength is 0 or less.
When your turn begins, place 1 virus counter on Parasite.)
  end
end
