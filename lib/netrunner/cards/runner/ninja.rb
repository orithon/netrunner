# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Ninja < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Ninja'
    @subtypes = [Subtype::ICEBREAKER, Subtype::KILLER]
    @faction = Faction::Runner::CRIMINAL
    @cost = 4
    @influence_cost = 2
    @ram_cost = 1
    @strength = 0
    @text = %(1{credit}: Break <b>sentry</b> subroutine.
3{credit}: +5 strength.)
    @flavour_text = 'You feel Ninja before you see Ninja, if you see Ninja at all.'
  end
end
