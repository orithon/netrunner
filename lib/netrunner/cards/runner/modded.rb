# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/subtype'
require 'netrunner/faction'

class Modded < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Modded'
    @subtypes = [Subtype::MOD]
    @faction = Faction::Runner::SHAPER
    @cost = 0
    @influence_cost = 2
    @text = 'Install a program or a piece of hardware, lowering the install cost by 3.'
    @flavour_text = "There's no replacement for a home-grown program. Fed on late nights, oaty bars, and single-minded determination. Cheaper, too."
  end
end
