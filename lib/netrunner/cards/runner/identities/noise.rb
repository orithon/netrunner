# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/identity'
require 'netrunner/subtype'
require 'netrunner/faction'

class NoiseHackerExtraordinaire < RunnerCard
  include Identity::Runner

  def initialize_attributes
    super

    @name = 'Noise: Hacker Extraordinaire'
    @display_name = 'Noise'
    @flavour_name = 'Hacker Extraordinaire'
    @subtypes = [Subtype::G_MOD]
    @faction = Faction::Runner::ANARCH
    @link = 0
    @deck_size = 45
    @influence = 15
    @text = 'Whenever you install a <b>virus</b> program, the Corp trashes the top card of R&D.'
    @flavour_text = %("Watch this. It'll be funny.")
  end
end
