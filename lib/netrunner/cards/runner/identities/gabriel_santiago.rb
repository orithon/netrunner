# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/identity'
require 'netrunner/subtype'
require 'netrunner/faction'

class GabrielSantiagoConsummateProfessional < RunnerCard
  include Identity::Runner

  def initialize_attributes
    super

    @name = 'Gabriel Santiago: Consummate Professional'
    @display_name = 'Gabriel Santiago'
    @flavour_name = 'Consummate Professional'
    @subtypes = [Subtype::CYBORG]
    @faction = Faction::Runner::CRIMINAL
    @link = 0
    @deck_size = 45
    @influence = 15
    @text = 'The first time you make a successful run on HQ each turn, gain 2{credit}.'
    @flavour_text = %("Of course I steal from the rich. They're the ones with all the money.")
  end
end
