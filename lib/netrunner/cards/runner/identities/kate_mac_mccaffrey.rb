# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/identity'
require 'netrunner/subtype'
require 'netrunner/faction'

class KateMacMccaffreyDigitalTinker < RunnerCard
  include Identity::Runner

  def initialize_attributes
    super

    @name = 'Kate "Mac" McCaffrey: Digital Tinker'
    @display_name = 'Kate "Mac" McCaffrey'
    @flavour_name = 'Digital Tinker'
    @subtypes = [Subtype::NATURAL]
    @faction = Faction::Runner::SHAPER
    @link = 1
    @deck_size = 45
    @influence = 15
    @text = 'Lower the install cost of the first program or piece of hardware you install each turn by 1.'
    @flavour_text = '"Are you listening?"'
  end
end
