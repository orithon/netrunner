# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class SacrificialConstruct < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Sacrificial Construct'
    @subtypes = [Subtype::REMOTE]
    @faction = Faction::Runner::SHAPER
    @cost = 0
    @influence_cost = 1
    @text = '{trash}: Prevent an installed program or an installed piece of hardware from being trashed.'
    @flavour_text = 'The life expectancy of a jacked construct is about that of a mayfly. In other words, short.'
  end
end
