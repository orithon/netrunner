# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/subtype'
require 'netrunner/faction'

class InsideJob < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Inside Job'
    @subtypes = [Subtype::RUN]
    @faction = Faction::Runner::CRIMINAL
    @cost = 2
    @influence_cost = 3
    @text = 'Make a run. Bypass the first piece of ice encountered during this run.'
    @flavour_text = %("Hey, listen, I'm not asking you to do anything dangerous. Just let me into the building. And tell me which room has the weakest security. And please don't say 'the bathroom' again.")
  end
end
