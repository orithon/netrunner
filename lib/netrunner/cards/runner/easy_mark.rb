# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'
require 'netrunner/subtype'

class EasyMark < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Easy Mark'
    @subtypes = [Subtype::JOB]
    @faction = Faction::Runner::CRIMINAL
    @cost = 0
    @influence_cost = 1
    @text = 'Gain 3{credit}.'
    @flavour_text = '"Hey kid, you fire up that now, bound to be vamped real bad. Some real pathetic individuals around here. But thankfully I got just the ticket..."'
  end
end
