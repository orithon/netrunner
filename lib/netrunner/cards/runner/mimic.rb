# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Mimic < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Mimic'
    @subtypes = [Subtype::ICEBREAKER, Subtype::KILLER]
    @faction = Faction::Runner::ANARCH
    @cost = 3
    @influence_cost = 1
    @ram_cost = 1
    @strength = 3
    @text = '1{credit}: Break <b>sentry</b> subroutine.'
    @flavour_text = 'November 5th: the day when all would see the corrupt machinations of the corporate oligarchy.'
  end
end
