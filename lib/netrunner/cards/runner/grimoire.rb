# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/unique'
require 'netrunner/faction'
require 'netrunner/subtype'

class Grimoire < RunnerCard
  include Hardware
  include Unique

  def initialize_attributes
    super

    @name = 'Grimoire'
    @subtypes = [Subtype::CONSOLE]
    @faction = Faction::Runner::ANARCH
    @cost = 3
    @influence_cost = 2
    @text = %(+2{ram}
Whenever you install a <b>virus</b> program, place 1 virus counter on that program.
Limit 1 <b>console</b> per player.)
    @flavour_text = '"My little book of magic spells." - Whizzard'
  end
end
