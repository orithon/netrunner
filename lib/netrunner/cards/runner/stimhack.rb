# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/subtype'
require 'netrunner/faction'

class Stimhack < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Stimhack'
    @subtypes = [Subtype::RUN]
    @faction = Faction::Runner::ANARCH
    @cost = 0
    @influence_cost = 1
    @text = 'Make a run, and gain 9{credit}, which you may use only during this run. After the run is completed, suffer 1 brain damage (cannot be prevented) and return to the bank any of the 9{credit} not spent.'
  end
end
