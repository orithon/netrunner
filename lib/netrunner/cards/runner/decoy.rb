# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class Decoy < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Decoy'
    @subtypes = [Subtype::CONNECTION]
    @faction = Faction::Runner::CRIMINAL
    @cost = 1
    @influence_cost = 2
    @text = '{trash}: Avoid receiving 1 tag.'
    @flavour_text = %("I get the feeling that this is the wrong place, Frank."
"What makes you say that, D?"
"The curlers.")
  end
end
