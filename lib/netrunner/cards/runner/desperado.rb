# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/unique'
require 'netrunner/faction'
require 'netrunner/subtype'

class Desperado < RunnerCard
  include Hardware
  include Unique

  def initialize_attributes
    super

    @name = 'Desperado'
    @subtypes = [Subtype::CONSOLE]
    @faction = Faction::Runner::CRIMINAL
    @cost = 3
    @influence_cost = 3
    @text = %(+1{ram}
Gain 1{credit} whenever you make a successful run.
Limit 1 <b>console</b> per player.)
  end
end
