# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class CrashSpace < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Crash Space'
    @subtypes = [Subtype::LOCATION]
    @faction = Faction::Runner::CRIMINAL
    @cost = 2
    @influence_cost = 2
    @text = %(2{recurring_credit}
Use these credits to pay for removing tags.
{trash}: Prevent up to 3 meat damage.)
    @flavour_text = %("My roomie, he had a cousin with a college girlfriend whose brother's best friend was an addict, and the addict's mother used to live here. So yeah, there's a connection."
\t- Lez "Rockfist" S.)
  end
end
