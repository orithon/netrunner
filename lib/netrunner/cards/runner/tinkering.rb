# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/subtype'
require 'netrunner/faction'

class Tinkering < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Tinkering'
    @subtypes = [Subtype::MOD]
    @faction = Faction::Runner::SHAPER
    @cost = 0
    @influence_cost = 4
    @text = 'Choose a piece of ice. That ice gains <b>sentry</b>, <b>code gate</b>, and <b>barrier</b> until the end of the turn.'
    @flavour_text = %("There's that moment, you know, when the whole world seems to fall away and it is only you and your mod, and the mod is the world.")
  end
end
