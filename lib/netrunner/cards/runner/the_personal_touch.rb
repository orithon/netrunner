# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/faction'
require 'netrunner/subtype'

class ThePersonalTouch < RunnerCard
  include Hardware

  def initialize_attributes
    super

    @name = 'The Personal Touch'
    @subtypes = [Subtype::MOD]
    @faction = Faction::Runner::SHAPER
    @cost = 2
    @influence_cost = 2
    @text = %(Install The Personal Touch only on an <b>icebreaker</b>.
Host <b>icebreaker</b> has +1 strength.)
    @flavour_text = 'A z-loop here, a cortical wave there...'
  end
end
