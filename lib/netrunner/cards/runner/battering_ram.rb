# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class BatteringRam < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Battering Ram'
    @subtypes = [Subtype::ICEBREAKER, Subtype::FRACTER]
    @faction = Faction::Runner::SHAPER
    @cost = 5
    @influence_cost = 2
    @ram_cost = 2
    @strength = 3
    @text = %(2{credit}: Break up to 2 <b>barrier</b> subroutines.
1{credit}: +1 strength for the remainder of this run.)
    @flavour_text = %("It's called 'brute-forcing' and it's just as effective today as it was a hundred years ago."
\t- Kate "Mac" McCaffrey)
  end
end
