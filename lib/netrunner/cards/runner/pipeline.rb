# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class Pipeline < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Pipeline'
    @subtypes = [Subtype::ICEBREAKER, Subtype::KILLER]
    @faction = Faction::Runner::SHAPER
    @cost = 3
    @influence_cost = 1
    @ram_cost = 1
    @strength = 1
    @text = %(1{credit}: Break <b>sentry</b> subroutine.
2{credit}: +1 strength for the remainder of this run.)
  end
end
