# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/icebreaker'
require 'netrunner/subtype'
require 'netrunner/faction'

class FemmeFatale < RunnerCard
  include Icebreaker

  def initialize_attributes
    super

    @name = 'Femme Fatale'
    @subtypes = [Subtype::ICEBREAKER, Subtype::KILLER]
    @faction = Faction::Runner::CRIMINAL
    @cost = 9
    @influence_cost = 1
    @ram_cost = 1
    @strength = 2
    @text = %(1{credit}: Break <b>sentry</b> subroutine.
2{credit}: +1 strength.
When you install Femme Fatale, choose an installed piece of ice. When you encounter that ice, you may pay 1{credit} per subroutine on that ice to bypass it.)
  end
end
