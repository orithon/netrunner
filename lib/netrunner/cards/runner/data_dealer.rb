# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/resource'
require 'netrunner/faction'
require 'netrunner/subtype'

class DataDealer < RunnerCard
  include Resource

  def initialize_attributes
    super

    @name = 'Data Dealer'
    @subtypes = [Subtype::CONNECTION, Subtype::SEEDY]
    @faction = Faction::Runner::CRIMINAL
    @cost = 0
    @influence_cost = 2
    @text = '<b>Forfeit an agenda</b>, {click}: Gain 9{credit}.'
    @flavour_text = "Shadier the dealer, better the price. Unless the dealer's too shady. Then their might be a hidden fee after they take your scrip."
  end
end
