# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/hardware'
require 'netrunner/faction'
require 'netrunner/subtype'

class AkamatsuMemChip < RunnerCard
  include Hardware

  def initialize_attributes
    super

    @name = 'Akamatsu Mem Chip'
    @subtypes = [Subtype::CHIP]
    @faction = Faction::Runner::SHAPER
    @cost = 1
    @influence_cost = 1
    @text = '+1{ram}'
    @flavour_text = 'The Akamatsu company was founded on three principles: first, to make the fastest mem chips on the market, second, to turn a profit, and third, to serve as a front for the manufacture of illegal neural-stimulants. It is the last principle that perhaps explains their rabid brand loyalty.'
  end
end
