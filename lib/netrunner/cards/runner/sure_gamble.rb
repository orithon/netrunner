# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/event'
require 'netrunner/faction'

class SureGamble < RunnerCard
  include Event

  def initialize_attributes
    super

    @name = 'Sure Gamble'
    @faction = Faction::Runner::NEUTRAL
    @cost = 5
    @influence_cost = 0
    @text = 'Gain 9{credit}.'
    @flavour_text = 'Lady Luck took the form of a hifi quantum manipulation ring that she wore on her middle finger.'
  end
end
