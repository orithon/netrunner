# frozen_string_literal: true

require 'netrunner/piece/card/runner_card'
require 'netrunner/piece/card/program'
require 'netrunner/faction'

class MagnumOpus < RunnerCard
  include Program

  def initialize_attributes
    super

    @name = 'Magnum Opus'
    @faction = Faction::Runner::SHAPER
    @cost = 5
    @influence_cost = 2
    @ram_cost = 2
    @text = '{click}: Gain 2{credit}.'
    @flavour_text = 'The Great Work was completed on a rainy Thursday afternoon. There were no seismic shifts, no solar flares, no sign from the earth or heavens that the world had changed. But upstalk in Heinlein, on a single Cybsoft manufactured datacore, the flickering data quantums of an account began to fill with creds. Real, honest-to-goodness UN certified creds.'
  end
end
