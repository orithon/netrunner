# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class Enigma < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Enigma'
    @subtypes = [Subtype::CODE_GATE]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 3
    @influence_cost = 0
    @strength = 2
    @text = %({subroutine} The Runner loses {click}, if able.
{subroutine} End the run.)
    @flavour_text = %("Hey, hey! Wake up man. You were under a long time. What'd you see?"
"I... don't remember.")
  end
end
