# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class WallOfThorns < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Wall of Thorns'
    @subtypes = [Subtype::BARRIER, Subtype::AP]
    @faction = Faction::Corporation::JINTEKI
    @cost = 8
    @influence_cost = 1
    @strength = 5
    @text = %({subroutine} Do 2 net damage.
{subroutine} End the run.)
    @flavour_text = '"Most runners do their business in full-sim, with their rigs wired directly into their brains. The setup has a large number of advantages, with the runner able to process data and input commands far faster than a traditional meat-bound system. But it also means greater risk."'
  end
end
