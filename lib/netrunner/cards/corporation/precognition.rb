# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/operation'
require 'netrunner/faction'

class Precognition < CorporateCard
  include Operation

  def initialize_attributes
    super

    @name = 'Precognition'
    @faction = Faction::Corporation::JINTEKI
    @cost = 0
    @influence_cost = 3
    @text = 'Look at the top 5 cards of R&D and arrange them in any order.'
    @flavour_text = '"There was a new texture in her phantom cortex. It had always been there, she realized. It was everything and nothing."'
  end
end
