# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/operation'
require 'netrunner/faction'
require 'netrunner/subtype'
require 'netrunner/event'

class HedgeFund < CorporateCard
  include Operation

  def initialize_attributes
    super

    @name = 'Hedge Fund'
    @subtypes = [Subtype::TRANSACTION]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 5
    @influence_cost = 0
    @text = 'Gain 9{credit}'
    @flavour_text = %(Hedge Fund. Noun. An ingenious device by which the rich get richer while every other poor SOB is losing his shirt.
\t-The Anarch's Dictionary, Volume Who's Counting?)
  end

  def play
    super
    deliver(message: Event::CORP_EARN_CREDITS, payload: 9)
  end
end
