# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class Chum < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Chum'
    @subtypes = [Subtype::CODE_GATE]
    @faction = Faction::Corporation::JINTEKI
    @cost = 1
    @influence_cost = 1
    @strength = 4
    @text = '{subroutine} The next piece of ice the Runner encounters during this run has +2 strength. Do 3 net damage unless the Runner breaks all subroutines on that piece of ice.'
    @flavour_text = %("You ever get that feeling like you're shark food? Pay attention to that feeling."
\t- Ji "Noise" Reilly)
  end
end
