# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/asset'
require 'netrunner/faction'
require 'netrunner/subtype'

class ProjectJunebug < CorporateCard
  include Asset

  def initialize_attributes
    super

    @name = 'Project Junebug'
    @subtypes = [Subtype::AMBUSH, Subtype::RESEARCH]
    @faction = Faction::Corporation::JINTEKI
    @cost = 0
    @influence_cost = 1
    @trash_cost = 0
    @text = %(Project Junebug can be advanced.
If you pay 1{credit} when the Runner accesses Project Junebug, do 2 net damage for each advancement token on Project Junebug.)
  end
end
