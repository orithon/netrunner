# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/asset'
require 'netrunner/faction'

class ZaibatsuLoyalty < CorporateCard
  include Asset

  def initialize_attributes
    super

    @name = 'Zaibatsu Loyalty'
    @faction = Faction::Corporation::JINTEKI
    @cost = 0
    @influence_cost = 1
    @trash_cost = 4
    @text = %(If the Runner is about to expose a card, you may rez Zaibatsu Loyalty.
1{credit} or {trash}: Prevent 1 card from being exposed.)
  end
end
