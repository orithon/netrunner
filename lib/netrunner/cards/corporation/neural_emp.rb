# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/operation'
require 'netrunner/faction'
require 'netrunner/subtype'

class NeuralEmp < CorporateCard
  include Operation

  def initialize_attributes
    super

    @name = 'Neural EMP'
    @subtypes = [Subtype::GRAY_OPS]
    @faction = Faction::Corporation::JINTEKI
    @cost = 2
    @influence_cost = 2
    @text = %(Play only if the Runner made a run during his or her last turn.
Do 1 net damage.)
    @flavour_text = %("The trick isn't hitting the person you were aiming at. It's hitting only the person you were aiming at.")
  end
end
