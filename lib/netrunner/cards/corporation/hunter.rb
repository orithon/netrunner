# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class Hunter < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Hunter'
    @subtypes = [Subtype::SENTRY, Subtype::TRACER, Subtype::OBSERVER]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 1
    @influence_cost = 0
    @strength = 4
    @text = '{subroutine} <b>Trace<sup>3</sup></b> - If successful, give the Runner 1 tag.'
    @flavour_text = %(.//run/hunter-tr/return=true
client/sec256IPv7->confirm? /y
3926:0HB7:1001:2NB1:1601:7784:ERROR)
  end
end
