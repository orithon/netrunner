# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class DataMine < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Data Mine'
    @subtypes = [Subtype::TRAP, Subtype::AP]
    @faction = Faction::Corporation::JINTEKI
    @cost = 0
    @influence_cost = 2
    @strength = 2
    @text = '{subroutine} Do 1 net damage. Trash Data Mine.'
    @flavour_text = '"Access HarmlessFile.datZ -> Are you sure? y/n"'
  end
end
