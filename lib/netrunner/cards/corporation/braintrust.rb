# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/agenda'
require 'netrunner/faction'
require 'netrunner/subtype'

class Braintrust < CorporateCard
  include Agenda

  def initialize_attributes
    super

    @name = 'Braintrust'
    @subtypes = [Subtype::RESEARCH]
    @faction = Faction::Corporation::JINTEKI
    @cost = 3
    @score = 2
    @text = %(When you score Braintrust, place 1 agenda counter on it for every 2 advancement tokens on it over 3.
The rez cost of all ice is lowered by 1 for each agenda counter on Braintrust.)
  end
end
