# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class NeuralKatana < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Neural Katana'
    @subtypes = [Subtype::SENTRY, Subtype::AP]
    @faction = Faction::Corporation::JINTEKI
    @cost = 4
    @influence_cost = 2
    @strength = 3
    @text = '{subroutine} Do 3 net damage.'
    @flavour_text = %(Forged by Ak.wa on 23.11.79-23. Filed 23.11.79-23.2 with #34k-lw3-21HH-4i.
//Samurai included.)
  end
end
