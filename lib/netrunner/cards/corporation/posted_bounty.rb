# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/agenda'
require 'netrunner/faction'
require 'netrunner/subtype'

class PostedBounty < CorporateCard
  include Agenda

  def initialize_attributes
    super

    @name = 'Posted Bounty'
    @subtypes = [Subtype::SECURITY]
    @faction = Faction::Corporation::WEYLAND
    @cost = 3
    @score = 1
    @text = 'When you score Posted Bounty, you may forfeit it to give the Runner 1 tag and take 1 bad publicity.'
    @flavour_text = '"If some two-cred newsy picks it up, even better. The scum could be in the alleys of Guayaquil or the slums of BosWash. Not to mention off-planet."'
  end
end
