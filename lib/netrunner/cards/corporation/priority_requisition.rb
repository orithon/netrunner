# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/agenda'
require 'netrunner/faction'
require 'netrunner/subtype'

class PriorityRequisition < CorporateCard
  include Agenda

  def initialize_attributes
    super

    @name = 'Priority Requisition'
    @subtypes = [Subtype::SECURITY]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 5
    @score = 3
    @text = 'When you score Priority Requisition, you may rez a piece of ice ignoring all costs.'
    @flavour_text = %("If it isn't in my terminal by six p.m., heads are going to roll!")
  end
end
