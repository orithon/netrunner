# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/asset'
require 'netrunner/faction'

class PadCampaign < CorporateCard
  include Asset

  def initialize_attributes
    super

    @name = 'PAD Campaign'
    @subtypes = [Subtype::ADVERTISEMENT]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 2
    @influence_cost = 0
    @trash_cost = 4
    @text = 'Gain 1{credit} when your turn begins.'
    @flavour_text = 'It is like the one you just bought, only better.'
  end
end
