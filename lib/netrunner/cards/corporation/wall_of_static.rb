# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class WallOfStatic < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Wall of Static'
    @subtypes = [Subtype::BARRIER]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 3
    @influence_cost = 0
    @strength = 3
    @text = '{subroutine} End the run.'
    @flavour_text = %("There's nothing worse than seeing that beautiful blue ball of data just out of reach as your connection derezzes. I think they do it just to taunt us." - Ele "Smoke" Scovak)
  end
end
