# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/asset'
require 'netrunner/faction'

class MelangeMiningCorp < CorporateCard
  include Asset

  def initialize_attributes
    super

    @name = 'Melange Mining Corp.'
    @faction = Faction::Corporation::NEUTRAL
    @cost = 1
    @influence_cost = 0
    @trash_cost = 1
    @text = '{click}, {click}, {click}: Gain 7{credit}.'
    @flavour_text = %("The mining bosses are worse than any downstalk crime lords. Tri-Maf, 4K, Yak, I don't care what gangs you got down there. In Heinlein there's just one law: the He3 must flow."
\t- "Old" Rick Henry, escaped clone.)
  end
end
