# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/agenda'
require 'netrunner/faction'
require 'netrunner/subtype'

class NiseiMkIi < CorporateCard
  include Agenda

  def initialize_attributes
    super

    @name = 'Nisei MK II'
    @subtypes = [Subtype::INITIATIVE]
    @faction = Faction::Corporation::JINTEKI
    @cost = 4
    @score = 1
    @text = %(Place 1 agenda counter on Nisei MK II when you score it.
<strong>Hosted agenda counter</strong>: End the run.)
  end
end
