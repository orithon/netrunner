# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/identity'
require 'netrunner/faction'
require 'netrunner/subtype'

class JintekiPersonalEvolution < CorporateCard
  include Identity::Corporation

  def initialize_attributes
    super

    @name = 'Jinteki: Personal Evolution'
    @display_name = 'Jinteki'
    @flavour_name = 'Personal Evolution'
    @subtypes = [Subtype::MEGACORP]
    @faction = Faction::Corporation::JINTEKI
    @deck_size = 45
    @influence = 15
    @text = 'Whenever an agenda is scored or stolen, do 1 net damage.'
    @flavour_text = '"When You Need the Human Touch."'
  end
end
