# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/asset'
require 'netrunner/faction'
require 'netrunner/subtype'

class Snare < CorporateCard
  include Asset

  def initialize_attributes
    super

    @name = 'Snare!'
    @subtypes = [Subtype::AMBUSH]
    @faction = Faction::Corporation::JINTEKI
    @cost = 0
    @influence_cost = 2
    @trash_cost = 0
    @text = %(If Snare! is accessed from R&D, the Runner must reveal it.
If you pay 4{credit} when the Runner accesses Snare!, do 3 net damage and give the Runner 1 tag. Ignore this effect if the Runner accesses Snare! from Archives.)
  end
end
