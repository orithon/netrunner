# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/upgrade'
require 'netrunner/unique'
require 'netrunner/faction'
require 'netrunner/subtype'

class AkitaroWatanabe < CorporateCard
  include Upgrade
  include Unique

  def initialize_attributes
    super

    @name = 'Akitaro Watanabe'
    @subtypes = [Subtype::SYSOP, Subtype::UNORTHODOX]
    @faction = Faction::Corporation::JINTEKI
    @cost = 1
    @influence_cost = 2
    @trash_cost = 3
    @text = 'The rez cost of ice protecting this server is lowered by 2.'
    @flavour_text = %("Just don't ask how he does it.")
  end
end
