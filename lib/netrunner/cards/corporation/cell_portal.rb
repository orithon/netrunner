# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/ice'
require 'netrunner/faction'
require 'netrunner/subtype'

class CellPortal < CorporateCard
  include Ice

  def initialize_attributes
    super

    @name = 'Cell Portal'
    @subtypes = [Subtype::CODE_GATE, Subtype::DEFLECTOR]
    @faction = Faction::Corporation::JINTEKI
    @cost = 5
    @influence_cost = 2
    @strength = 7
    @text = '{subroutine} The Runner approaches the outermost piece of ice protecting the attacked server. Derez Cell Portal.'
    @flavour_text = '"Where does it go?"'
  end
end
