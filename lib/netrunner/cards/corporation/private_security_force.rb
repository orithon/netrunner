# frozen_string_literal: true

require 'netrunner/piece/card/corporate_card'
require 'netrunner/piece/card/agenda'
require 'netrunner/faction'
require 'netrunner/subtype'

class PrivateSecurityForce < CorporateCard
  include Agenda

  def initialize_attributes
    super

    @name = 'Private Security Force'
    @subtypes = [Subtype::SECURITY]
    @faction = Faction::Corporation::NEUTRAL
    @cost = 4
    @score = 2
    @text = 'If the Runner is tagged, Private Security Force gains: "{click}: Do 1 meat damage."'
    @flavour_text = %("Expensive? Not when you're protecting a fortune as large as ours.")
  end
end
