# frozen_string_literal: true

require 'netrunner/messaging/messenger'
require 'netrunner/messaging/collectable'
require 'netrunner/piece/moveable'

# What I am looking for out of pieces?
# 1. Pieces exist in named zones, but those zones are mostly just areas in the
#    game board.  Zones aren't on pieces, per say, they are simply areas on the
#    table top.  For example, we have HQ, the Stack, and the Heap.  Pieces may
#    exist in these zones, as cards in the corporation's hand are said to be in
#    HQ, or cards in the runner's discard pile are said to be in the Heap.
#    Pieces don't have to be a zone themselves, as they can exist hosted on
#    other pieces.  For example, a Parasite can only be installed on a rezzed
#    piece of Ice.  The Parasite's zone will ultimately be its parent's zone.
# 2. Following with Parasite's example, pieces can host other pieces.  A piece
#    doesn't need to be subdivided into named zones, its list of children can be
#    treated like a bag or a stack.  It is simply a collection of pieces, and we
#    can determine what is there by iterating over it and inspecting the pieces.
#    Named zones could help, but the name needn't align with what's in the zone!
# 3. Pieces can move between zones.
# 4. Likewise, pieces can move between parents!

# Hmm... Pieces don't need to be able to host other pieces.  Maybe there could
# be a module for that?  At their simplest, a Piece is a token, like a credit,
# that simply exists somewhere in the board state.  Cards need to be able to
# host other pieces, though.

# A game piece.  Could be just about anything in a board / card game, from the
# smallest token to the board itself.
# Pieces are always Messengers, capable of sending and receiving messages.
# Pieces can host other pieces in named "zones", and thus can themselves be
# hosted on other pieces.
class Piece
  include Messenger
  include Moveable
  include Collectable

  def initialize
    initialize_attributes
    initialize_move_subscriptions
    subscribe(message: "fetch_#{address}", action: :fetch)
  end

  def initialize_attributes; end

  private

  def fetch(message)
    message.payload = self
  end
end
