# frozen_string_literal: true

module Zone
  # This zone is where spent credits are moved to. By moving spent credits here they are automatically unsubscribed from
  # the events in their zone.
  BANK = :bank

  module Corporation
    CREDITS = :corporation_credits
    DECK = :research_and_development
    HAND = :headquarters
    IDENTITY = :corporation_identity
    DISCARD = :archives
  end

  module Runner
    CREDITS = :runner_credits
    DECK = :stack
    DISCARD = :heap
    HAND = :grip
    IDENTITY = :runner_identity
    PLAY_AREA = :rig
  end

  IN_PLAY = [
    Corporation::IDENTITY,
    Runner::IDENTITY,
    Runner::PLAY_AREA
  ].freeze
end
