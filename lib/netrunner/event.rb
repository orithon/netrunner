# frozen_string_literal: true

# A collection of pre-defined game events, messages that will be delivered to
# any game objects that are listening. This is not an exhaustive list of events
# that can occur in the game, but is the list of events required to perform key
# game functions. Individual cards may define their own events.
module Event
  # Basic engine events
  CHOICE = :choice     # Give the active player a set of actions to choose from
  ORDERING = :ordering # Give the active player a set of actions to order

  # Event for initial game setup
  GAME_SETUP = :game_setup

  # Event for actions that occur at the start of the game, but after setup.
  GAME_START = :game_start

  # Corporation phase events
  BEGIN_CORP_ACTION = :begin_corp_action
  BEGIN_CORP_DISCARD = :begin_corp_discard
  BEGIN_CORP_DRAW = :begin_corp_draw
  BEGIN_CORP_TURN = :begin_corp_turn
  CORP_ABILITY_REZ = :corp_ability_rez
  CORP_ABILITY_REZ_SCORE = :corp_ability_rez_score
  CORP_ACTION = :corp_action
  CORP_DISCARD = :corp_discard
  CORP_DRAW = :corp_draw
  END_CORP_ACTION = :end_corp_action
  END_CORP_DISCARD = :end_corp_discard
  END_CORP_DRAW = :end_corp_draw
  END_CORP_TURN = :corp_end_turn

  # Runner phase events
  BEGIN_RUNNER_ACTION = :begin_runner_action
  BEGIN_RUNNER_START = :begin_runner_start
  BEGIN_RUNNER_TURN = :begin_runner_turn
  END_RUNNER_ACTION = :end_runner_action
  END_RUNNER_DISCARD = :end_runner_discard
  END_RUNNER_TURN = :end_runner_turn
  BEGIN_RUNNER_DISCARD = :begin_runner_discard
  END_RUNNER_START = :end_runner_start
  RUNNER_ABILITY_REZ = :runner_ability_rez
  RUNNER_ACTION = :runner_action
  RUNNER_DISCARD = :runner_discard

  # Corporation basic actions
  CORP_DRAW_ACTION = :corp_draw_action
  CORP_GAIN_CREDIT_ACTION = :corp_gain_credit_action
  CORP_INSTALL_ACTION = :corp_install_action
  CORP_PLAY_OPERATION_ACTION = :corp_play_operation_action
  CORP_ADVANCE_ACTION = :corp_advance_action
  CORP_PURGE_ACTION = :corp_purge_action
  CORP_TRASH_RESOURCE_ACTION = :corp_trash_resource_action

  # Runner basic actions
  RUNNER_DRAW_ACTION = :runner_draw_action
  RUNNER_GAIN_CREDIT_ACTION = :runner_gain_credit_action
  RUNNER_INSTALL_ACTION = :runner_install_action
  RUNNER_PLAY_EVENT_ACTION = :runner_play_event_action
  RUNNER_REMOVE_TAG_ACTION = :runner_remove_tag_action
  RUNNER_RUN_ACTION = :runner_run_action

  # Corporation events
  CORP_SPEND_CLICKS = :corp_spend_clicks
  CORP_EARN_CREDITS = :corp_earn_credits

  # Runner events
  RUNNER_SPEND_CLICKS = :runner_spend_clicks
  RUNNER_EARN_CREDITS = :runner_earn_credits

  module Aggregation
    PLAYABLE_OPERATION_AGGREGATION = :playable_operation_aggregation

    IN_CORPORATION_HAND_AGGREGATION = :in_corporation_hand_aggregation
    IN_CORPORATION_DECK_AGGREGATION = :in_corporation_deck_aggregation
    CORPORATION_CREDITS_AGGREGATION = :corporation_credits_aggregation

    IN_RUNNER_HAND_AGGREGATION = :in_runner_hand_aggregation
    IN_RUNNER_DECK_AGGREGATION = :in_runner_deck_aggregation
    RUNNER_CREDITS_AGGREGATION = :runner_credits_aggregation
    RUNNER_LINK = :runner_link

    module Runner
      INSTALLABLE = :installable
    end

    module Corporation
      INSTALLABLE = :installable
    end
  end
end
