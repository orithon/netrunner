# frozen_string_literal: true

require 'netrunner/messaging/messenger'
require 'netrunner/messaging/choice'
require 'netrunner/phases/phase'
require 'netrunner/phases/phase_manager'

class Game
  class InvalidMoveError < RuntimeError; end

  include Messenger

  attr_reader :corporation, :runner, :phase, :stack

  def initialize(corporation:, runner:)
    @corporation = corporation
    @runner = runner
    self.phase = Phase::GAME_START
    subscribe(message: Event::CHOICE, action: :push_payload)
    deliver(message: Event::GAME_SETUP)
  end

  def process(message = nil)
    handle_user_choice(message) unless message.nil?

    loop do
      increment_phase if @stack.size.zero?

      if @stack.last.is_a?(Choice)
        break @stack.last unless @stack.last.choices.count > 1 && @stack.last.choices.uniq.one?

        # If all the choices are the same, then there's no choice at all...
        choices = @stack.pop.choices
        choices.each { |choice| @stack << choice }
      end

      deliver(message: @stack.pop)
    end
  end

  def phase=(phase)
    @phase = phase
    @stack = @phase.events.reverse
  end

  def increment_phase(force = false)
    self.phase = if player_action_phase_finished? || force
                   phase_manager.next
                 else
                   phase_manager.current
                 end
  end

  def fetch_object(address)
    deliver(message: "fetch_#{address}", payload: nil).payload
  end

  def fetch_objects(addresses)
    addresses.map { |address| fetch_object(address) }
  end

  private

  def corporation_action_phase_finished?
    phase.name == "Corporation's Action Phase" && @corporation.clicks.zero?
  end

  def runner_action_phase_finished?
    phase.name == "Runner's Action Phase" && @runner.clicks.zero?
  end

  def player_action_phase_finished?
    corporation_action_phase_finished? && runner_action_phase_finished?
  end

  def phase_manager
    @phase_manager ||= PhaseManager.new(
      Phase::CORP_DRAW,
      Phase::CORP_ACTION,
      Phase::CORP_DISCARD,
      Phase::RUNNER_START,
      Phase::RUNNER_ACTION,
      Phase::RUNNER_DISCARD
    )
  end

  def push_payload(message)
    @stack << message.payload
  end

  def handle_user_choice(event)
    raise InvalidMoveError unless (top = @stack.last).include?(event)

    top.remove(event)

    # Defer removing from the stack until we're sure there's no error.
    @stack.pop if top.count.zero?
    @stack << event
  end
end
