# frozen_string_literal: true

require 'netrunner/piece'
require 'netrunner/piece/credit'
require 'netrunner/piece/click'
require 'netrunner/event'

require 'forwardable'

# This class represents a player in the game of Netrunner.
# It has a deck, a hand of cards, and a pool of credits.
# Players are "Pieces" in that they are considered a part of the game, and
# contain part of the physical game state.
class Player < Piece
  extend Forwardable

  INITIAL_CLICKS = 0
  ACTION_EVENT = nil

  def_delegator :@deck, :identity
  attr_reader :deck, :hand

  def initialize(deck)
    @deck = deck
    @credits = []
    @clicks = []
    @hand = []
    subscribe(message: Event::GAME_SETUP, action: :setup)
    subscribe(message: self.class::ACTION_EVENT, action: :handle_action)
  end

  def setup(_message)
    deck.shuffle
    gain_credits(5)
    reload_clicks
    draw_cards(5)
    identity.play
    setup_basic_action_handlers
  end

  def credits
    # First remove credits that have been spent.
    # When credits are spent this array isn't modified, they only move themselves to the bank.
    @credits.select! { |credit| credit.zone == credit_pool_zone }
    @credits.count
  end

  def clicks
    @clicks.count
  end

  def gain_credits(amount)
    execute_repeatedly(:gain_credit, amount)
  end

  def gain_credit
    @credits << Credit.new.move(credit_pool_zone)
  end

  def lose_credits(amount)
    execute_repeatedly(:lose_credit, amount)
  end

  def lose_credit
    @credits.pop.spend
  end

  def reload_clicks
    until clicks_reloaded? do gain_click end
  end

  def clicks_reloaded?
    clicks == self.class::INITIAL_CLICKS
  end

  def gain_clicks(amount)
    execute_repeatedly(:gain_click, amount)
  end

  def gain_click
    @clicks << Click.new
  end

  def lose_clicks(amount)
    execute_repeatedly(:lose_click, amount)
  end

  def lose_click
    @clicks.pop
  end

  def draw_cards(amount)
    execute_repeatedly(:draw_card, amount)
  end

  def draw_card
    @hand << deck.draw
  end

  private

  def setup_basic_action_handlers; end

  def handle_action(_message)
    deliver(message: Event::CHOICE, payload: self.class::BASIC_ACTIONS)
  end

  def handle_spend_clicks(message)
    lose_clicks(message.payload)
  end

  def handle_earn_credits(message)
    gain_credits(message.payload)
  end

  def execute_repeatedly(action, times)
    (1..times).each { |_| send(action) }
  end

  def credit_pool_zone
    raise NotImplementedError
  end

  def credit_aggregation_event
    raise NotImplementedError
  end
end
