# frozen_string_literal: true

module Type
  IDENTITY = 'Identity'

  module Corporation
    AGENDA = 'Agenda'
    ASSET = 'Asset'
    ICE = 'Ice'
    OPERATION = 'Operation'
    UPGRADE = 'Upgrade'
  end

  module Runner
    EVENT = 'Event'
    HARDWARE = 'Hardware'
    PROGRAM = 'Program'
    RESOURCE = 'Resource'
  end
end
