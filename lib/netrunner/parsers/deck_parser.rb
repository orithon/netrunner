# frozen_string_literal: true

require 'netrunner/piece/deck'
require 'netrunner/piece/card'

class DeckParser
  class NoSuchIdentityError < RuntimeError; end
  class EmptyFileError < RuntimeError; end
  class InvalidCardsError < RuntimeError; end

  def initialize(deck_data)
    @deck_data = deck_data
  end

  def call
    raise EmptyFileError if @deck_data.empty?
    @call ||= build_deck
  end

  private

  def build_deck
    # Create an empty array (the deck).
    # Turn the deck listing into a hash, with each card name as the key and the
    # amount as the value.
    # For each card name:
    # 1. Transform it into the name of the card class.
    # 2. Get the class.
    # 3. Create a number of instances of the class equal to the amount, and
    #    store them in the deck.
    cards = []
    card_listing.each do |card, amount|
      amount.times { cards << card.new }
    end

    Deck.new(identity, cards).tap(&:valid?)
  end

  def identity
    @identity ||= parse_identity
  end

  def parse_identity
    identity_name = @deck_data.lines.first
    identity_class_name = Card.class_name_from_card_name(identity_name)
    begin
      identity_card = Object.const_get(identity_class_name).new
    rescue NameError
      raise NoSuchIdentityError
    end
    raise NoSuchIdentityError if identity_card.type != 'Identity'
    identity_card
  end

  def card_listing
    card_listing = {}
    # Exclude the first row since that's always the identity.
    begin
      @deck_data.lines.drop(1).each do |line|
        amount, card_name = line.scan(/(\d*)? ?(.*)/).first
        card_class_name = Card.class_name_from_card_name(card_name)
        card_class = Object.const_get(card_class_name)
        card_listing[card_class] = normalize_amount(amount)
      end
    rescue NameError
      raise InvalidCardsError
    end
    card_listing
  end

  def normalize_amount(amount)
    if !amount.empty?
      amount.to_i
    else
      1
    end
  end
end
