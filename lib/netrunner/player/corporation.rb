# frozen_string_literal: true

require 'netrunner/player'
require 'netrunner/event'
require 'netrunner/zone'
require 'netrunner/messaging/choice'

class Corporation < Player
  INITIAL_CLICKS = 3
  ACTION_EVENT = Event::CORP_ACTION
  BASIC_ACTIONS = Choice.new([Event::CORP_DRAW_ACTION,
                              Event::CORP_GAIN_CREDIT_ACTION,
                              Event::CORP_INSTALL_ACTION,
                              Event::CORP_PLAY_OPERATION_ACTION,
                              Event::CORP_ADVANCE_ACTION,
                              Event::CORP_PURGE_ACTION,
                              Event::CORP_TRASH_RESOURCE_ACTION])

  private

  def setup_basic_action_handlers
    super

    subscribe(message: Event::CORP_SPEND_CLICKS, action: :handle_spend_clicks)
    subscribe(message: Event::CORP_EARN_CREDITS, action: :handle_earn_credits)

    subscribe(message: Event::CORP_PLAY_OPERATION_ACTION, action: :handle_corp_play_operation_action)
  end

  def handle_corp_play_operation_action(_message)
    message = deliver(message: Event::Aggregation::PLAYABLE_OPERATION_AGGREGATION, payload: [])
    deliver(message: Event::CORP_SPEND_CLICKS, payload: 1)
    deliver(message: Event::CHOICE, payload: Choice.new(message.payload.map(&:address)))
  end

  def credit_pool_zone
    Zone::Corporation::CREDITS
  end

  def credit_aggregation_event
    Event::Aggregation::CORPORATION_CREDITS_AGGREGATION
  end
end
