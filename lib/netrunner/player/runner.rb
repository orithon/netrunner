# frozen_string_literal: true

require 'netrunner/player'
require 'netrunner/event'
require 'netrunner/zone'
require 'netrunner/messaging/choice'

class Runner < Player
  INITIAL_CLICKS = 4
  ACTION_EVENT = Event::RUNNER_ACTION
  BASIC_ACTIONS = Choice.new([Event::RUNNER_DRAW_ACTION,
                              Event::RUNNER_GAIN_CREDIT_ACTION,
                              Event::RUNNER_INSTALL_ACTION,
                              Event::RUNNER_PLAY_EVENT_ACTION,
                              Event::RUNNER_REMOVE_TAG_ACTION,
                              Event::RUNNER_RUN_ACTION])

  def link
    deliver(message: Event::Aggregation::RUNNER_LINK, payload: 0).payload
  end

  private

  def setup_basic_action_handlers
    super

    subscribe(message: Event::RUNNER_SPEND_CLICKS, action: :handle_spend_clicks)
    subscribe(message: Event::RUNNER_INSTALL_ACTION, action: :handle_runner_install_action)
  end

  def handle_runner_install_action(_message)
    message = deliver(message: Event::Aggregation::Runner::INSTALLABLE, payload: [])
    deliver(message: Event::RUNNER_SPEND_CLICKS, payload: 1)
    deliver(message: Event::CHOICE, payload: Choice.new(message.payload.map(&:address)))
  end

  def credit_pool_zone
    Zone::Runner::CREDITS
  end

  def credit_aggregation_event
    Event::Aggregation::RUNNER_CREDITS_AGGREGATION
  end
end
