# frozen_string_literal: true

require 'netrunner/piece'

# Represents a click in the game of Netrunner.
# Clicks are tokens that can be used to execute actions.
class Click < Piece; end
