# frozen_string_literal: true

module Moveable
  attr_reader :zone

  def zone_subscriptions
    Hash.new { |hash, key| hash[key] = [] }
  end

  def initialize_move_subscriptions
    subscribe(message: move_message, action: :handle_move)
  end

  def handle_move(message)
    handle_zone_subscriptions(@zone, :unsubscribe_from_zone)
    handle_zone_subscriptions(message.payload, :subscribe_to_zone)
    @zone = message.payload
  end

  def move(zone)
    deliver(message: move_message, payload: zone)
    self
  end

  private

  def move_message
    "move_#{address}"
  end

  def handle_zone_subscriptions(zone, action)
    return unless zone_subscriptions.key?(zone)

    zone_subscriptions[zone].each { |zone_subscription| send(action, zone_subscription) }
  end

  def subscribe_to_zone(zone_subscription)
    handle_subscription_with_hash(:subscribe, zone_subscription)
  end

  def unsubscribe_from_zone(zone_subscription)
    handle_subscription_with_hash(:unsubscribe, zone_subscription)
  end

  def handle_subscription_with_hash(action, hash)
    raise unless hash.key?(:message) && hash.key?(:action)

    send(action, **hash)
  end
end
