# frozen_string_literal: true

require 'netrunner/piece'
require 'netrunner/event'

# Represents a credit in the game of Netrunner.
# Credits are tokens that can be used to pay for stuff.
class Credit < Piece
  # Used to indicate that an implementation of Credit is a recurring credit, that is, one which is returned to the
  # player at the beginning of their turn.
  module Recurring; end

  def initialize
    super
    subscribe(message: address, action: :handle_spend)
  end

  def hash
    # We want to consider credits to be the same if they are of the same class.
    self.class.hash ^ :credit.hash
  end

  def eql?(other)
    hash == other.hash
  end

  def ==(other)
    eql?(other)
  end

  def zone_subscriptions
    super.tap do |subs|
      subs[Zone::Corporation::CREDITS] << { message: Event::Aggregation::CORPORATION_CREDITS_AGGREGATION, action: :collect }
      subs[Zone::Runner::CREDITS] << { message: Event::Aggregation::RUNNER_CREDITS_AGGREGATION, action: :collect }
    end
  end

  def handle_spend(_message)
    move(Zone::BANK)
  end

  def spend
    deliver(message: address)
  end

  def spent?
    zone == Zone::BANK
  end
end
