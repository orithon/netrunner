# frozen_string_literal: true

require 'netrunner/piece'
require 'netrunner/zone'

class Card < Piece
  attr_reader :name,
              :faction,
              :subtypes,
              :text,
              :flavour_text,
              :limit

  def self.class_name_from_card_name(card_name)
    card_name.asciify.strip.alphanumericize.camelize
  end

  def initialize_attributes
    super

    @name = ''
    @faction = ''
    @subtypes = []
    @text = ''
    @flavour_text = ''
    @limit = 3
  end

  def unique?
    false
  end

  def play; end

  def in_play?
    Zone::IN_PLAY.include?(@zone)
  end

  def display_name
    if @display_name.nil?
      name
    else
      @display_name
    end
  end

  def address
    "#{name.asciify.strip.alphanumericize.snakeize}_#{super}"
  end
end
