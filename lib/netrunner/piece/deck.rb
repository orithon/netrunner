# frozen_string_literal: true

# Represents a deck in the game of Netrunner.
# Decks have many rules surrounding their validity, and this class has checks
# for (hopefully) all of them.
# Decks can (and should!) be shuffled, and can have cards drawn from them.

# TODO: make event-driven
class Deck
  class MoreThanOneIdentityError < RuntimeError; end
  class RunnerDeckContainsCorporationCardsError < RuntimeError; end
  class CorporationDeckContainsRunnerCardsError < RuntimeError; end
  class TooFewCardsError < RuntimeError; end
  class TooManyCardCopiesError < RuntimeError; end
  class TooManyOutOfFactionCardsError < RuntimeError; end
  class FactionSpecificCardsMismatchError < RuntimeError; end
  class TooFewAgendaPointsError < RuntimeError; end
  class TooManyAgendaPointsError < RuntimeError; end

  attr_reader :identity, :cards

  def initialize(identity, cards)
    @identity = identity
    @cards = []
    cards.each { |card| add_to_deck(card) }
    @shuffled = false
  end

  def valid?
    raise MoreThanOneIdentityError if identity_in_cards?
    raise RunnerDeckContainsCorporationCardsError if runner? && identity_card_mismatch?
    raise CorporationDeckContainsRunnerCardsError if corporation? && identity_card_mismatch?
    raise TooFewCardsError if too_few_cards?
    raise TooManyCardCopiesError if too_many_card_copies?
    raise TooManyOutOfFactionCardsError if too_many_out_of_faction_cards?
    raise FactionSpecificCardsMismatchError if out_of_faction_no_influence_cards?
    raise TooFewAgendaPointsError if corporation? && too_few_agenda_points?
    raise TooManyAgendaPointsError if corporation? && too_many_agenda_points?
  end

  def shuffle
    cards.shuffle!
    @shuffled = true
  end

  def shuffled?
    @shuffled
  end

  def draw
    @cards.shift.tap { |card| card.move(hand_zone_name) }
  end

  def add_to_deck(card)
    @cards.insert(0, card)
    card.move(deck_zone_name)
  end

  private

  def deck_zone_name
    return Zone::Corporation::DECK if corporation?
    return Zone::Runner::DECK if runner?

    raise 'Unrecognized identity'
  end

  def hand_zone_name
    return Zone::Corporation::HAND if corporation?
    return Zone::Runner::HAND if runner?

    raise 'Unrecognized identity'
  end

  def runner?
    identity.role == 'Runner'
  end

  def corporation?
    identity.role == 'Corporation'
  end

  def identity_in_cards?
    cards.select { |card| card.type == 'Identity' }.any?
  end

  def identity_card_mismatch?
    cards.reject { |card| card.role == identity.role }.any?
  end

  def too_few_cards?
    cards.count < identity.deck_size
  end

  def too_many_card_copies?
    cards.group_by(&:class).select do |klass, instances|
      instances.count > klass.new.limit
    end.any?
  end

  def too_many_out_of_faction_cards?
    influence_costs = out_of_faction_cards.map(&:influence_cost).compact
    total_influence_cost = influence_costs.reduce(0, :+)
    total_influence_cost > identity.influence
  end

  def out_of_faction_no_influence_cards?
    out_of_faction_cards.select do |card|
      card.influence_cost.nil?
    end.any?
  end

  def too_few_agenda_points?
    total_points < expected_agenda_points[0]
  end

  def too_many_agenda_points?
    total_points > expected_agenda_points[1]
  end

  def out_of_faction_cards
    valid_factions = [identity.faction, 'Neutral']
    @out_of_faction_cards ||= cards.reject do |card|
      valid_factions.include?(card.faction)
    end
  end

  def agenda_cards
    @agenda_cards ||= cards.select do |card|
      card.type == 'Agenda'
    end
  end

  def expected_agenda_points
    @expected_agenda_points ||= [
      (cards.count / 5).to_i * 2,
      (cards.count / 5).to_i * 2 + 1
    ]
  end

  def total_points
    @total_points ||= agenda_cards.map(&:score).reduce(0, :+)
  end
end
