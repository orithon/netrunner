# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/event'
require 'netrunner/zone'
require 'netrunner/piece/card/payable'

# Represents an Operation card in the game of Netrunner.
# An Operation is a card that can be played by the Corporation for a 1-time effect.
module Operation
  include Payable::Corporation

  attr_reader :influence_cost

  def initialize
    super
    subscribe(message: address, action: :handle_play)
  end

  def zone_subscriptions
    # TODO: ensure that this only gathers playable operations (those the player can afford, etc.)
    super.tap do |subs|
      subs[Zone::Corporation::HAND] << { message: Event::Aggregation::PLAYABLE_OPERATION_AGGREGATION, action: :collect }
    end
  end

  def handle_play(_message)
    pay(@cost)
    play
    discard
  end

  def discard
    move(Zone::Corporation::DISCARD)
  end

  def type
    Type::Corporation::OPERATION
  end

  # TODO: method to determine if an operation is playable.
  # An operation is playable if the corporation player has enough credits to play it, including any credits meant for
  # specifically for playing operations, minus any penalties to the cost of operations.
  # Although the penalty should probably be implemented as a modifier on operation cost.
end
