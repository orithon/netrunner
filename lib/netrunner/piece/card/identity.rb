# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/zone'
require 'netrunner/piece/card/provides_link'

module Identity
  module CommonIdentity
    IDENTITY_ZONE = nil

    attr_reader :flavour_name, :deck_size, :influence

    def play
      super
      move(self.class::IDENTITY_ZONE)
    end

    def type
      Type::IDENTITY
    end
  end

  module Corporation
    include CommonIdentity

    IDENTITY_ZONE = Zone::Corporation::IDENTITY
  end

  module Runner
    include CommonIdentity
    include ProvidesLink

    IDENTITY_ZONE = Zone::Runner::IDENTITY
  end
end
