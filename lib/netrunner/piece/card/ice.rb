# frozen_string_literal: true

require 'netrunner/type'

module Ice
  attr_reader :cost, :influence_cost, :strength

  def type
    Type::Corporation::ICE
  end
end
