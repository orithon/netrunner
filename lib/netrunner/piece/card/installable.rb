# frozen_string_literal: true

require 'netrunner/event'
require 'netrunner/zone'

module Installable
  module CommonInstallable
    AGGREGATION_EVENT = nil

    def zone_subscriptions
      super.tap do |subs|
        subs[self.class::HAND_ZONE] << { message: self.class::AGGREGATION_EVENT, action: :collect }
      end
    end
  end

  module Corporation
    include CommonInstallable

    HAND_ZONE = Zone::Corporation::HAND
    AGGREGATION_EVENT = Event::Aggregation::Corporation::INSTALLABLE
  end

  module Runner
    include CommonInstallable

    HAND_ZONE = Zone::Runner::HAND
    AGGREGATION_EVENT = Event::Aggregation::Runner::INSTALLABLE
  end
end
