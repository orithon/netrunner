# frozen_string_literal: true

require 'netrunner/event'

module ProvidesLink
  def zone_subscriptions
    super.tap do |subs|
      subs[Zone::Runner::PLAY_AREA] << { message: Event::Aggregation::RUNNER_LINK, action: :collect_link }
      subs[Zone::Runner::IDENTITY] << { message: Event::Aggregation::RUNNER_LINK, action: :collect_link }
    end
  end

  def collect_link(message)
    raise NotImplementedError if @link.nil?

    message.payload = message.payload + @link
  end
end
