# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/piece/card/payable'

module Event
  include Payable::Runner

  attr_reader :influence_cost

  def type
    Type::Runner::EVENT
  end
end
