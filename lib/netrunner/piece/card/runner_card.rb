# frozen_string_literal: true

require 'netrunner/piece/card'

class RunnerCard < Card
  def zone_subscriptions
    super.tap do |subs|
      subs[Zone::Runner::HAND] << { message: Event::Aggregation::IN_RUNNER_HAND_AGGREGATION, action: :collect }
      subs[Zone::Runner::DECK] << { message: Event::Aggregation::IN_RUNNER_DECK_AGGREGATION, action: :collect }
    end
  end

  def role
    'Runner'
  end
end
