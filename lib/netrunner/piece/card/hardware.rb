# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/piece/card/payable'
require 'netrunner/piece/card/installable'

module Hardware
  include Payable::Runner
  include Installable::Runner

  attr_reader :influence_cost

  def type
    Type::Runner::HARDWARE
  end

  def initialize
    super
    subscribe(message: address, action: :handle_play)
  end

  def handle_play(_message)
    pay(@cost)
    play
  end

  def play
    super
    move(Zone::Runner::PLAY_AREA)
  end
end
