# frozen_string_literal: true

require 'netrunner/piece/card'

class CorporateCard < Card
  def zone_subscriptions
    super.tap do |subs|
      subs[Zone::Corporation::HAND] << { message: Event::Aggregation::IN_CORPORATION_HAND_AGGREGATION, action: :collect }
      subs[Zone::Corporation::DECK] << { message: Event::Aggregation::IN_CORPORATION_DECK_AGGREGATION, action: :collect }
    end
  end

  def role
    'Corporation'
  end
end
