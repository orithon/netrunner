# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/piece/card/payable'

module Upgrade
  include Payable::Corporation

  attr_reader :influence_cost, :trash_cost

  def type
    Type::Corporation::UPGRADE
  end
end
