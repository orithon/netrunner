# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/piece/card/program'

module Icebreaker
  include Program

  attr_reader :strength
end
