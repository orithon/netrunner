# frozen_string_literal: true

require 'netrunner/type'

module Asset
  attr_reader :cost, :influence_cost, :trash_cost

  def type
    Type::Corporation::ASSET
  end
end
