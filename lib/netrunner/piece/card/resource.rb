# frozen_string_literal: true

require 'netrunner/type'
require 'netrunner/piece/card/payable'

module Resource
  include Payable::Runner

  attr_reader :influence_cost

  def type
    Type::Runner::RESOURCE
  end
end
