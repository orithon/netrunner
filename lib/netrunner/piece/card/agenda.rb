# frozen_string_literal: true

require 'netrunner/type'

module Agenda
  attr_reader :cost, :score

  def influence_cost
    nil
  end

  def type
    Type::Corporation::AGENDA
  end
end
