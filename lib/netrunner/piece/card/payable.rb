# frozen_string_literal: true

require 'netrunner/event'

module Payable
  module CommonPayable
    attr_reader :cost

    CREDITS_AGGREGATION_EVENT = nil

    def pay(amount)
      credits = available_credits

      if credits.uniq.one?
        credits.each(&:spend)
      else
        deliver(message: Event::CHOICE, payload: Choice.new(available_credits.map(&:address), amount))
      end
    end

    def available_credits
      targetted_payload = TargettedPayload.new(target: self)
      deliver(message: self.class::CREDITS_AGGREGATION_EVENT, payload: targetted_payload).payload
    end
  end

  module Corporation
    include CommonPayable

    CREDITS_AGGREGATION_EVENT = Event::Aggregation::CORPORATION_CREDITS_AGGREGATION
  end

  module Runner
    include CommonPayable

    CREDITS_AGGREGATION_EVENT = Event::Aggregation::RUNNER_CREDITS_AGGREGATION
  end
end
