# frozen_string_literal: true

module Collectable
  def collect(message)
    message.payload << self
  end
end
