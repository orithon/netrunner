class Subscription
  attr_reader :message, :receiver, :action

  def initialize(message:, receiver:, action:)
    @message = message
    @receiver = receiver
    @action = action
  end

  def receive(some_message)
    receiver.send(action, some_message) if matches?(some_message)
  end

  def matches?(some_message)
    message == some_message.type
  end

  def ==(other)
    other.respond_to?(:message) &&
      other.respond_to?(:receiver) &&
      other.respond_to?(:action) &&
      other.message == message &&
      other.receiver == receiver &&
      other.action == action
  end

  def eql?(other)
    self == other
  end

  def hash
    message.hash ^ receiver.object_id ^ action.hash
  end

  def to_s
    "#{self.class}<message=#{message}, receiver=#{receiver}, action=#{action}>"
  end
end
