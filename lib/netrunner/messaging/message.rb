# frozen_string_literal: true

class Message
  attr_reader :type
  attr_accessor :payload

  def initialize(type:, payload: nil)
    @type = type
    @payload = payload
  end

  def to_s
    "#{self.class}<type=#{type}, payload=#{payload}>"
  end
end
