# frozen_string_literal: true

class TargettedPayload < Array
  attr_reader :target

  def initialize(*args, target:)
    super(*args)
    @target = target
  end
end
