# frozen_string_literal: true

require 'forwardable'

# Represents a list of choices that the player must make one or more of. By default the player only has to choose one of
# them (a simple choice), but they may have to choose all of them (an ordering of events), or only some of them
# (choosing which credits to spend on something)
class Choice
  extend Forwardable

  attr_reader :choices

  def_delegators :@choices, :include?, :delete_at, :index, :count, :map

  def initialize(choices = [], amount_to_select = 1)
    @choices = choices
    @amount_to_select = amount_to_select

    raise if @amount_to_select < 1
    raise if @choices.count < 1
    raise if @amount_to_select > @choices.count
  end

  def remove(item)
    delete_at(index(item))

    @choices.clear if (@amount_to_select -= 1).zero?
  end
end
