# frozen_string_literal: true

require 'singleton'
require 'set'

class SubscriptionManager
  include Singleton
  attr_reader :subscriptions, :log

  def initialize
    clear
  end

  def clear
    @subscriptions ||= Set.new
    @subscriptions.subtract(subscriptions)
    @log = []
  end

  def add(subscription)
    @subscriptions.add(subscription)
  end

  def remove(subscription)
    @subscriptions.delete(subscription)
  end

  def deliver(message)
    @log << message.to_s

    # iterate over a clone so that new subscriptions can be added during
    # iteration without ill-effect.
    subscriptions.clone.each { |subscription| subscription.receive(message) }
    message
  end
end
