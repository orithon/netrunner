# frozen_string_literal: true

require 'netrunner/messaging/subscription_manager'
require 'netrunner/messaging/subscription'
require 'netrunner/messaging/message'

module Messenger
  def deliver(message:, payload: nil)
    subscription_manager.deliver(Message.new(type: message, payload: payload))
  end

  def subscribe(message:, action:)
    subscription_manager.add(subscription(message, action))
  end

  def unsubscribe(message:, action:)
    subscription_manager.remove(subscription(message, action))
  end

  def address
    object_id
  end

  private

  def subscription_manager
    SubscriptionManager.instance
  end

  def subscription(message, action)
    Subscription.new(message: message, receiver: self, action: action)
  end
end
