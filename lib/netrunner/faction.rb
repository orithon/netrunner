# frozen_string_literal: true

module Faction
  module Corporation
    HAAS_BIOROID = 'Haas-Bioroid'
    JINTEKI = 'Jinteki'
    NBN = 'NBN'
    WEYLAND = 'Weyland Consortium'
    NEUTRAL = 'Neutral'
  end

  module Runner
    ANARCH = 'Anarch'
    CRIMINAL = 'Criminal'
    SHAPER = 'Shaper'
    NEUTRAL = 'Neutral'
  end
end
