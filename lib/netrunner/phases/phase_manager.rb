# frozen_string_literal: true

class PhaseManager
  def initialize(*phases)
    @phases = phases
    @current_phase_index = -1
  end

  def current
    @phases[@current_phase_index]
  end

  def next
    @current_phase_index = (@current_phase_index + 1) % @phases.size
    current
  end
end
