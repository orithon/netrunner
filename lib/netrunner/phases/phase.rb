# frozen_string_literal: true

require 'netrunner/event'

# A Phase represents a period in the turn structure of a game of Netrunner.
# Each phase has a name, and consists of a series of events. The first event in
# each phase indicates the start of the phase, and the last event indicates the
# end of the phase. The middle events are considered to be the body.
class Phase
  attr_reader :name, :events

  def initialize(name, *events)
    @name = name
    @events = *events
  end

  def start
    events.first
  end

  def end
    events.last
  end

  def body
    events[1...-1]
  end

  # Basic game phases
  GAME_START = new('Game Start Phase',
                   Event::GAME_START)

  CORP_DRAW = new("Corporation's Draw Phase",
                  Event::BEGIN_CORP_DRAW,           # Corp's draw phase begins
                  Event::CORP_ABILITY_REZ_SCORE,    # Paid abilities can be triggered, Cards can be rezzed, Agendas can be scored
                  Event::BEGIN_CORP_TURN,           # Turn begins ("When your turn begins" conditionals meet their trigger conditions)
                  Event::CORP_DRAW,                 # Draw one card
                  Event::CORP_ABILITY_REZ_SCORE,    # Paid abilities can be triggered, Cards can be rezzed, Agendas can be scored
                  Event::END_CORP_DRAW)             # Corp's draw phase ends

  CORP_ACTION = new("Corporation's Action Phase",
                    Event::BEGIN_CORP_ACTION,       # Corp's action phase begins
                    Event::CORP_ACTION,             # Take an action
                    Event::CORP_ABILITY_REZ_SCORE,  # Paid abilities can be triggered, Cards can be rezzed, Agendas can be scored
                    Event::END_CORP_ACTION)         # Corp's action phase ends (return to step 1 if the corp has more actions)

  CORP_DISCARD = new("Corporation's Discard Phase",
                     Event::BEGIN_CORP_DISCARD,     # Corp's discard phase begins
                     Event::CORP_DISCARD,           # Discard down to maximum hand size
                     Event::CORP_ABILITY_REZ,       # Paid abilities can be triggered, Cards can be rezzed
                     Event::END_CORP_TURN,          # End of turn
                     Event::END_CORP_DISCARD)       # Corp's discard phase ends

  RUNNER_START = new("Runner's Start Phase",
                     Event::BEGIN_RUNNER_START,     # Runner's start phase begins
                     Event::RUNNER_ABILITY_REZ,     # Paid abilities can be triggered, Cards can be rezzed
                     Event::BEGIN_RUNNER_TURN,      # Turn begins ("When your turn begins" conditionals meet their trigger conditions)
                     Event::RUNNER_ABILITY_REZ,     # Paid abilities can be triggered, Cards can be rezzed
                     Event::END_RUNNER_START)       # Runner's start phase ends

  RUNNER_ACTION = new("Runner's Action Phase",
                      Event::BEGIN_RUNNER_ACTION,   # Runner's action phase begins
                      Event::RUNNER_ACTION,         # Take an action
                      Event::RUNNER_ABILITY_REZ,    # Paid abilities can be triggered, Cards can be rezzed
                      Event::END_RUNNER_ACTION)     # Runner's action phase ends (return to step 1 if the runner has more actions)

  RUNNER_DISCARD = new("Runner's Discard Phase",
                       Event::BEGIN_RUNNER_DISCARD, # Runner's discard phase begins
                       Event::RUNNER_DISCARD,       # Discard down to maximum hand size
                       Event::RUNNER_ABILITY_REZ,   # Paid abilities can be triggered, Cards can be rezzed
                       Event::END_RUNNER_TURN,      # End of turn
                       Event::END_RUNNER_DISCARD)   # Runner's discard phase ends
end
