# frozen_string_literal: true

class String
  def camelize
    split(/[ _-]/).collect(&:capitalize).join
  end

  def snakeize
    split(/[ _-]/).collect(&:downcase).join('_')
  end

  def asciify
    unicode_normalize(:nfkd).encode('ASCII', replace: '')
  end

  def alphanumericize
    gsub(/[^A-Za-z0-9 ]/, '')
  end
end
